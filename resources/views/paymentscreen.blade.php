<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="/images/ico.png">
    <title>Formulario de Pago - {{ $comercio_name }}</title>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="/css/iofrm-style.css">
    <link rel="stylesheet" type="text/css" href="/css/iofrm-theme14.css">
</head>
<body>
<div class="form-body">
    <div class="row">
        <div class="form-holder">
            <div class="form-content">
                <div class="form-items">
                    <div class="website-logo-inside" style="text-align: center;">
                        <a href="/#">
                            <div class="logo">
                                <img class="" src="/images/logo.svg" alt="">
                            </div>
                        </a>
                    </div>
                    <label for="total"><strong>Total a Pagar:</strong></label>
                    <input name="total" type="text" class="form-control sm-content" value="Q. {{ $total }}" style="display: inline;margin-left: 15px;" disabled>
                    <form id="frmPago">
                    <input type="text" hidden="hidden" id="hash" value="{{$hash}}">
                        <div class="tab-content" id="stepsTabContent">
                            <div class="tab-pane fade show active" id="step1" role="tabpanel" aria-labelledby="step1-tab">
                                <div class="inline-el-holder">
                                    <div class="inline-el">
                                        <label>No. Orden: #{{$no_orden}}</label>
                                    </div>
                                    <div class="inline-el">
                                        <label>Página Web:  {{ $comercio_name }}</label>
                                    </div>
                                    <div class="">
                                        <label for="full_name">Nombre en la Orden:</label>
                                        <input name="full_name" type="text" class="form-control " value="{{ $first_name }} {{ $last_name }}" style="display: inline;" disabled>
                                    </div>
                                </div>
                                <div class="separator"></div>
                                @if($status == "pending")
                                <div class="form-sent show-it" style="display: contents">
                                    <div id="error" style="display: none;">
                                        <div class="tick-holder">
                                            <img src="/images/error.png" style="width: 20px">
                                        </div>
                                        <h3>Error al procesar pago</h3>
                                    </div>
                                </div>
                                <div id="pago">
                                    <label>Número de Tarjeta</label>
                                    <img src="/images/GeekPay-05.png" width="120px" "="" style="width: 120px;top: 10px;}max-height: none;">
                                    <div class="input-with-ccicon" style="margin-top: 10px;">
                                        <input name="cc" id="cc" type="number" class="form-control input-credit-card" placeholder="4321 1234 4321 1234">
                                        <i id="ccicon"></i>
                                    </div>
                                    @if( $cuotas == 1 )
                                    <div class="input-with-ccicon" style="margin-top: 10px;">
                                        <p class="form-row form-row-first" style="width:auto">
                                            <label for="visa_en_cuotas" id="name_visa_cuotas">Si deseas realizar tu pago en cuotas selecciona una opción.</label>
                                            <select name="nCuotas" id="nCuotas" class="form-control sm-content" style="width:35%; -webkit-appearance: menulist;margin-right:15px;
                                              -webkit-appearance: menulist;
                                              margin-right: 15px;
                                              -moz-appearance: menulist;">
                                                <option value="0" selected="">Al Contado</option>
                                                <option value="3">3</option>
                                                <option value="6">6</option>
                                                <option value="12">12</option>
                                            </select>
                                        </p>
                                    </div>
                                    @else
                                    <input type="text" hidden="hidden" id="nCuotas" value="0">
                                    @endif
                                    <div class="inline-el-holder">
                                        <div class="inline-el">
                                            <label>Fecha de expiración</label>
                                            <select name="t1"  id="t1" required="required" class="form-control sm-content">
                                                <option value="">Mes</option>
                                                <option value="01">01</option>
                                                <option value="02">02</option>
                                                <option value="03">03</option>
                                                <option value="04">04</option>
                                                <option value="05">05</option>
                                                <option value="06">06</option>
                                                <option value="07">07</option>
                                                <option value="08">08</option>
                                                <option value="09">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                        <div class="inline-el">
                                            <select name="t2" id="t2" required="required" class="form-control sm-content">
                                                <option value="">Año</option>
                                                <option value="20">2020</option><option value="21">2021</option><option value="22">2022</option><option value="23">2023</option><option value="24">2024</option><option value="25">2025</option><option value="26">2026</option><option value="27">2027</option><option value="28">2028</option><option value="29">2029</option><option value="30">2030</option>
                                            </select>
                                        </div>
                                        <div class="inline-el">
                                            <label>CVV</label>
                                            <input name="cvv" id="cvv" type="number" class="form-control sm-content input-credit-card" placeholder="123">
                                        </div>
                                    </div>
                                    <div class="form-button">
                                        <button id="pagar" class="ibtn">PAGAR</button>
                                    </div>
                                </div>

                                @else
                                <div class="form-sent show-it" style="display: contents">
                                    <div class="tick-holder">
                                        <div class="tick-icon"></div>
                                    </div>
                                    <h3>Pago ya Realizado o Cancelado</h3>
                                    <p>Por favor verifica con {{ $comercio_name }} </p>
                                </div>
                                @endif
                                <div class="form-sent show-it" style="display: contents">
                                    <div  id="complete" style="display: none">
                                        <div class="tick-holder">
                                            <div class="tick-icon"></div>
                                        </div>
                                        <h3>Pago Realizado Éxito</h3>
                                    </div>
                                </div>
                            </div>
                            <br /><br />
                            <div class="separator"></div>
                            <div class="inline-el-holder" style="text-align: center;">
                                <div class="">
                                    <label style="font-size: 11px;text-align: center">Derechos Reservados @ 2020 Mentes Geniales S.A.</label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/js/jquery.min.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/cleave.min.js"></script>
<script src="/js/main.js"></script>
</body>
</html>
