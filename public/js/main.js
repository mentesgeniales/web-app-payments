"use strict"

$(window).on("load", function() {
    $('.btn-forget').on('click',function(e){
        e.preventDefault();
       $('.form-items','.form-content').addClass('hide-it');
       $('.form-sent','.form-content').addClass('show-it');
    });
    $('.btn-tab-next').on('click',function(e){
        e.preventDefault();
        $('.nav-tabs .nav-item > .active').parent().next('li').find('a').trigger('click');
    });
    $('#pagar').on('click',function(e){
        $("#pago").hide();
        e.preventDefault()
        let data = {
            "cc": $("#cc").val() ,
            "t1": $("#t1").val(),
            "t2": $("#t2").val(),
            "cvv":$("#cvv").val(),
            "hash":$("#hash").val(),
            "cuotas":$("#nCuotas").val()
        };
        $.ajax({
            type: "POST",
            url: "https://app.centraldepago.com/api/integration/remotepay",
            data: data,
            success: function(r,v){
                if(r.responseCode == 200){
                    $("#error").hide();
                    $("#pago").hide();
                    $("#complete").show();
                }else{
                    $("#error").show();
                    $("#pago").show();
                }
            },
            dataType: "json"
        });
    });

});
