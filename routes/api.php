<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post ("/integration/" , "PaymentsController@process_payment");
Route::get ("/integration/" , "PaymentsController@process_payment");
Route::post ("/integration/remotepay" , "RemoteController@processPayment");
Route::post("/integration/a" , "BacPaymentsController@amexPay");


// to update departamentos - municipios y poblados
Route::get ("/departamentos/list/" , "SoapController@getDepartamentos");
Route::get ("/municipios/list/" , "SoapController@getMunicipios");
Route::get ("/poblados/list/" , "SoapController@getPoblados");


// para listar
Route::get('/filter/departamentos/' , 'CargoController@getDepartamentos');
Route::post('/filter/departamentos/' , 'CargoController@getDepartamentos');
Route::post('/filter/municipios/' , 'CargoController@filterMunicipios');
Route::post('/filter/poblados/' , 'CargoController@filterPoblados');

Route::get("/generar-guia/{comercio_id}/{order_id}/" , 'CargoController@generarGuia');
Route::get("/generar-manifiesto/{comercio_id}/" , 'CargoController@generarManifiesto');
Route::get("/manifiestos/list/{comercio_id}" , "CargoController@getManifiestos");

Route::post("/integration/epay/" , "ePayController@process_payment");
Route::get("/integration/w/message/" , "WhatsppController@send");

Route::get("/reportes/ventas/by-client/{id}/{year}/{month}" , "ReportesController@ventasByMonth" );
//Route::get("/integration/fel/comercio/{comercio_id}" , "");
//Route::post("/integration/fel/comercio" , "");

//

Route::post("/fel/process/" , "FEL@get_company_info");
Route::post("/fel/processbynit/" , "FEL@facturabynit");

Route::post("/pagos/index" , "ClientesFEL@index");


Route::post("/certificar/" , "FelController@certificar" );
Route::get("/fel/getnit/{nit}" , "FEL@get_nit_info");

Route::post("/certificar/pos" , "FelPosController@certificar");