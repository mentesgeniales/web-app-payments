<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class BannedIp extends Model {
    public function scopeToday($query){
        return $query->whereDate('created_at', '=', Carbon::today()->toDateString());
    }
}
