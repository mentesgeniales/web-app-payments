<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FelDato extends Model
{
    public static function get_company_data( $nit ){
        
    }
    
    public function comercio(){
        return $this->hasOne(Comercio::class , "id", "comercio_id");
    }
    
    
    public function regimen(){
        return $this->hasOne(Regimen::class, "id" , "regimen_tributario");
    }
}
