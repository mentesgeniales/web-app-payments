<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \MessageBirdClient;

class WhatsppController extends Controller {
    //
    public function send(){
        $messageBird = new MessageBirdClient('LBY4lYtSI6lHjCPV6H9v9Pqoi'); // Set your own API access key here.
        // Enable the whatsapp sandbox feature
        //$messageBird = new MessageBirdClient(
        //    'LBY4lYtSI6lHjCPV6H9v9Pqoi',
        //    null,
        //    [MessageBirdClient::ENABLE_CONVERSATIONSAPI_WHATSAPP_SANDBOX]
        //);
        $conversationId = '2e15efafec384e1c82e9842075e87beb';
        $content = new MessageBirdObjectsConversationContent();
        $content->text = '✅ You’ve successfully connected to MessageBird’s WhatsApp Sandbox! If you’re testing via the API Sandbox, head over to the WhatsApp Sandbox in our dashboard and send your first template message 🚀. Reply “stop” at any time to leave this WhatsApp Sandbox.';
        $message = new MessageBirdObjectsConversationMessage();
        $message->channelId = 'ad7048c7446b41debfd1d497ad519fe0';
        $message->type = 'text';
        $message->content = $content;
        try {
            $conversation = $messageBird->conversationMessages->create($conversationId, $message);
            var_dump($conversation);
        } catch (Exception $e) {
            echo sprintf("%s: %s", get_class($e), $e->getMessage());
        }
    }

}
