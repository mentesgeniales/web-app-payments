<?php /** @noinspection ALL */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departamento;
use App\Municipio;
use App\Poblado;
use App\Comercio;
use App\CredencialesComercio;
use App\CargoGuia;
use App\CargoManifiesto;
use App\Http\Controllers\SoapController;
class CargoController extends Controller {

    public function __construct() {
        $this->wc_api = "/wp-json/wc/v3/";
    }
    public function getDepartamentos(){
        $data = Departamento::all();
        return response()->json(["data" => $data]);
    }
    public function filterMunicipios(Request $request){
        $data = Municipio::where( 'CodigoDepto' , $request->input('codigo') )->get();
        return response()->json(["data" => $data]);
    }
    public function filterPoblados(Request $request){
        $data = Poblado::where( 'CodigoDepto' , $request->input('codigoDep') )->where('CodigoMunicipio' , $request->input('codigoMun'))
            ->get();
        return response()->json(["data" => $data]);
    }
    public function generarGuia($comercio_id , $order_id){
        if(!isset($comercio_id ) )
            return false;
        if(!isset($order_id))
            return false;
        $this->clean_comercio_id($comercio_id);

        if($this->comercio->integrations_type != 3 )
            return false;
        $cc = CredencialesComercio::where("comercio_id" ,"=",$this->comercio_id)->get();
        if(count ($cc) >= 1 ){
            $order = $this->curl($this->comercio->site_url . $this->wc_api . "orders/" . $order_id . "?consumer_key=" .$cc[0]->api_pub . "&consumer_secret=" . $cc[0]->api_pri );
            if($cc[0]->estado == 1){
                $env = "dev";
            }elseif($cc[0]->estado == 2){
                $env = "prod";
            }
            $guia = new SoapController(  $env , $cc[0]->usuario_caex , $cc[0]->clave_caex , $cc[0]->codigo_caex);
            $final_guia = $guia->registrarGuia($this->prepareOrderData($order));
            $rname = $this->tmp_storage($final_guia["link"] , $final_guia["no_guia"]);
            $headers = array(
                'Content-Type: application/pdf',
            );
            $file= storage_path(). "/app/public/tmp/" . $rname ;
            $save_guia = new CargoGuia();
                $save_guia->comercio_id = $comercio_id;
                $save_guia->order_id = $order_id;
                $save_guia->link_guia=$final_guia["link"];
                $save_guia->no_guia= $final_guia["no_guia"];
            $save_guia->save();
            $data = ["note" => $final_guia["no_guia"] ];
            $this->curl($this->comercio->site_url . $this->wc_api . "orders/" . $order_id ."/notes/". "?consumer_key=" .$cc[0]->api_pub . "&consumer_secret=" . $cc[0]->api_pri  , $data );
            return response()->download( $file, $rname ,$headers) ->deleteFileAfterSend(true);
        }else{
            return ["response_code" => 530 , "msg" => "Comercio no tiene acceso a esta función" ];
        }
    }
    public function generarManifiesto( $comercio_id ){
        if(!isset($comercio_id ) )
            return false;
        $this->clean_comercio_id($comercio_id);

        if($this->comercio->integrations_type != 3 )
            return false;
        $cc = CredencialesComercio::where("comercio_id" ,"=",$this->comercio_id)->get();
        if(count ($cc) >= 1 ){
            if($cc[0]->estado == 1){
                $env = "dev";
            }elseif($cc[0]->estado == 2){
                $env = "prod";
            }
            $manifiesto = new SoapController(  $env , $cc[0]->usuario_caex , $cc[0]->clave_caex , $cc[0]->codigo_caex);
            $date_init = date("Y-m-d", time() - 60 * 60 * 24) . 'T00:00:00';
            $date_end = date("c");
            $r = $manifiesto->getManifiesto(  $date_init , $date_end );
            $rname = $this->tmp_storage($r["link"] , "manifiesto");
            $headers = array(
                'Content-Type: application/pdf',
            );
            $file= storage_path(). "/app/public/tmp/" . $rname ;
            $save_guia = new CargoManifiesto();
            $save_guia->comercio_id = $this->comercio_id;
            $save_guia->link=$r["link"];
            $save_guia->save();
            return response()->download( $file, $rname ,$headers) ->deleteFileAfterSend(true);
            dd($r);
        }
    }

    public function getManifiestos($comercio_id){
        $this->clean_comercio_id($comercio_id);
        return ["data" => CargoManifiesto::where( "comercio_id", $this->comercio_id)->get()->sortByDesc("created_at") ];
    }

    private function prepareOrderData( $data ){
        $poblado = "";
        foreach ($data->meta_data as $key => $value){
            if($value->key =="_billing_downtown" )
                $poblado =$value->value;
        }
        //dd($data);
        if($data->payment_method == "geekpay"){
            $tipo_guia = 1;
        }else{
            $tipo_guia = 3;
        }
        $producto = "";
        foreach ( $data->line_items as $key => $value){
            //dd($value);
            $producto .= $value->name;
        }
        $producto_split = str_split($producto, 35);
        if(!isset($producto_split[1])){
            $producto_split[1] = "";
        }
        $rd = [
            "comercio_name" => $this->comercio->name,
            "order_id"  => $data->number,
            "name"      => $data->billing->first_name . ' ' . $data->billing->last_name,
            "address"   => $data->billing->address_1 .' '. $data->billing->address_2,
            "phone_number" => $data->billing->phone,
            "poblado"   => $poblado,
            "tipo_guia" => $tipo_guia,
            "total"     => $data->total,
            "productos" => $producto_split[0],
            "productos2"    => ($producto_split[1] ? $producto_split[1] : "")
        ];
        return $rd;
    }

    private function tmp_storage( $url , $name )    {

       $opts = [
            "http" => [
                "method" => "GET",
                "header" => "Host: www.te.com\r\n"
                    . "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:71.0) Gecko/20100101 Firefox/71.0\r\n"
                    . "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n"
                    . "Accept-Language: en-US,en;q=0.5\r\n"
                    . "Accept-Encoding: gzip, deflate, br\r\n"
            ],
        ];
        $context = stream_context_create($opts);
        $data = file_get_contents($url, false, $context);
        if($name=="manifiesto"){
            $name = $name . '.pdf';
        }else{
            $name = 'guia-' . $name . '.pdf';
        }
        \Storage::disk('tmp')->put( $name , $data);
        return $name;
    }
    public function clean_comercio_id ($comercio_id){
        $this->comercio_id = str_replace( "GT2020100" , "", $comercio_id);
        $this->comercio_id = (int) $this->comercio_id;
        $this->comercio = Comercio::find( $this->comercio_id);
    }

}
