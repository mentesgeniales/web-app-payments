<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\FelDato;
use App\FelRespuesta;
use App\ClientesFel;
use App\Comercio;
class FEL extends Controller {
    //
    
    public function __construct(){
    }
    
    public function get_nit_info($nit){
        $data = ["name" => "cf"];
        
                
        $url = "https://felgttestaws.digifact.com.gt/gt.com.fel.api.v3/api/SHAREDINFO?NIT=000044653948&DATA1=SHARED_GETINFONITcom&DATA2=NIT|".$nit."&USERNAME=USUARIOTEST";
        
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        $headers = array(
           "X-Custom-Header: value",
           "Content-Type: application/json",
           "Authorization:eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IkdULjAwMDA0NDY1Mzk0OC5VU1VBUklPVEVTVCIsIm5iZiI6MTY1Nzc2MjQyMiwiZXhwIjoxNjg4ODY2NDIyLCJpYXQiOjE2NTc3NjI0MjIsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDkyMjAiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQ5MjIwIn0.EumETeh94Zxw0uAcPZWAy5xCxHhyxOBF5H4U1LnCLzw"
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $resp = curl_exec($curl);
        $data = json_decode($resp);
        curl_close($curl);
        if($data->RESPONSE[0]->NOMBRE == "" ){
            if($nit == "cf" || $nit == "CF"){
                return "Consumidor Final";
            }else{
                return "INVALID";    
            }
            
        }else{
            return $data->RESPONSE[0]->NOMBRE;
        }
    }
    
    public function get_company_info(Request $request){
        $process_data = $request->all();
        $company_id = $process_data["comercio_id"];
        if(isset($process_data["ciudad"])){
            $direccionComprador = $process_data["ciudad"];
        }else{
            $direccionComprador = "Ciudad";
        }

      	if(isset($process_data["email"])){
            $email = $process_data["email"];
        }else{
            $email = "";
        }
        //$direccionComprador = ( $process_data["ciudad"] != "" ? $process_data["ciudad"] : "Ciudad" );
        //$descuento = ( $process_data["descuento"] != "" ? $process_data["ciudad"] : 0.00 );
        
        $found = FelRespuesta::where("comercio_id" , $company_id)->where("order_id", $process_data["order_id"])->get();
        if(count($found) > 0 ){
          		$find_cliente = ClientesFel::where("comercio_id" , $company_id)->where("order_id", $process_data["order_id"])->get();
            	$response_data = [];
                $response_data["AUTORIZACION"] = $found[0]->fel_autorizacion;
                $response_data["SERIE"] = $found[0]->fel_serie;
                $response_data["NUMERO"] = $found[0]->fel_correlativo	;
                $response_data["FECHA_EMISION"] = $found[0]->fel_fechaEmision ;
                if(count($find_cliente) >  0 ) {
                      $response_data["nit_cliente"] = $find_cliente[0]->nit_cliente ;
                      $response_data["nombre_cliente"] = $find_cliente[0]->nombre_cliente ;
                      $response_data["direccion_cliente"] = $find_cliente[0]->direccion_cliente ;
                      $response_data["email_cliente"] = $find_cliente[0]->email_cliente ;
                }
                return $response_data;
        }else{
            //dd($process_data);
          	if(isset($process_data["nit_cliente"] ) ){
            	$process_data["nit_cliente"] = $process_data["nit_cliente"];
              	$process_data["nombre_cliente"] = $process_data["nombre_cliente"];
              }else{
          		$process_data["nit_cliente"] = "CF";
              	$process_data["nombre_cliente"] = "Consumidor Final";    
          	}
         	
          	
        	$cliente_fel = new ClientesFel();
            $cliente_fel->order_id = $process_data["order_id"];
            $cliente_fel->comercio_id = $company_id;
            $cliente_fel->nit_cliente = $process_data["nit_cliente"];
            $cliente_fel->nombre_cliente = $process_data["nombre_cliente"];
            $cliente_fel->email_cliente = $email;
            $cliente_fel->direccion_cliente = $direccionComprador;
            $cliente_fel->save();
            $fel_datos = FelDato::where("comercio_id" , $company_id)->get()[0];
            //dd($company_id);
            //$token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IkdULjAwMDA0NDY1Mzk0OC5MYV9MZWNoaXRhIiwibmJmIjoxNjAwODcyOTM4LCJleHAiOjE2MzE5NzY5MzgsImlhdCI6MTYwMDg3MjkzOCwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo0OTIyMCIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDkyMjAifQ.m_N2ighEylTLeCo_Ism4iJExpXghKxRetWdwVrwACt4";
            //$token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IkdULjAwMDEwMDA2NDE4My5NRU5URVNfR0VOSUEiLCJuYmYiOjE2MjQzODUyMjYsImV4cCI6MTY1NTQ4OTIyNiwiaWF0IjoxNjI0Mzg1MjI2LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjQ5MjIwIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDo0OTIyMCJ9.ATVc2D9uYOT0Oiofdbo_T0XEy8jfFyTz1P1F-lMpj10";
            $token = $fel_datos->token;
            $min_nit = $fel_datos->nit;
            $l_c = strlen ($min_nit);
            //dd($l_c);
            $txt_nit = "";
            if( $l_c < 12) {
                $kk = 12 - $l_c;
                for ( $i = 0; $i < $kk; $i++){
                    $txt_nit = $txt_nit . "0";
                }
                $full_nit = $txt_nit.$min_nit;
            }else {
                $full_nit = $min_nit;
            }
            //dd($full_nit);
            //$full_nit = "000100064183";
            $user_name = $fel_datos->nombre_de_usuario;
            if($company_id == 61){
                $URL = "https://felgttestaws.digifact.com.gt/gt.com.fel.api.v3/api/FELRequestV2?NIT=000044653948&TIPO=CERTIFICATE_DTE_XML_TOSIGN&FORMAT=XML&USERNAME=USUARIOTEST";
            }else{
                $URL = "https://felgtaws.digifact.com.gt/gt.com.fel.api.v3/api/FELRequestV2?NIT=$full_nit&TIPO=CERTIFICATE_DTE_XML_TOSIGN&FORMAT=XML&USERNAME=$user_name";
            }
            date_default_timezone_set("America/Guatemala");
            $tipoDocumento = "FACT";
            $fechaEmision = date('Y-m-d');
            $horaEmision = date('H:m:s');
            $codigoMoneda = "GTQ";
            //DATOS DEL VENDEDOR
            $nitEmisor = $min_nit;
            
            $nombreEmisor = $fel_datos->company_name;
            
            
            
            if($company_id == 69){
                $codEstablecimiento = "3";    
            }else if($company_id == 55){
                    $codEstablecimiento = "3";    
                }else{
                    $codEstablecimiento = "1";
                }
            
            
            
            
            $nombreComercial = $fel_datos->company_name;
            
            
            
            if( $fel_datos->regimen_tributario == 4 ){
                $afiliacionIVA = "PEQ";
              	$tipo_dte = "FPEQ";
            }else{
                $afiliacionIVA = "GEN";    
              	$tipo_dte = "FACT";
            }
            
            $direccionEmisor = "CIUDAD";
            $codigoPostal = "01001";
            $municipio = "GUATEMALA";
            $depto = "GUATEMALA";
            $pais = "GT";
            //DATOS DEL COMPRADOR
            
            $nombreComprador = $process_data["nombre_cliente"];
            $nitComprador = $process_data["nit_cliente"];
            
            //$direccionComprador = "CIUDAD";
            $munComprador = "GUATEMALA";
            $deptoComprador = "GUATEMALA";
            $codigoPostalComprador = "01001";
            $paisComprador = "GT";
            //GENERACION DE XML
            $xml_data = '<?xml version="1.0" encoding="UTF-8"?><dte:GTDocumento Version="0.1" xmlns:dte="http://www.sat.gob.gt/dte/fel/0.2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <dte:SAT ClaseDocumento="dte">
                    <dte:DTE ID="DatosCertificados">
                        <dte:DatosEmision ID="DatosEmision">
                            <dte:DatosGenerales Tipo="' . $tipo_dte . '" FechaHoraEmision="' . $fechaEmision . 'T' . $horaEmision . '" CodigoMoneda="' . $codigoMoneda . '" />
                            <dte:Emisor NITEmisor="' . $nitEmisor . '" NombreEmisor="' . $nombreEmisor . '" CodigoEstablecimiento="' . $codEstablecimiento . '" NombreComercial="' . $nombreComercial . '" AfiliacionIVA="' . $afiliacionIVA . '">
                                <dte:DireccionEmisor>
                                    <dte:Direccion>' . $direccionEmisor . '</dte:Direccion>
                                    <dte:CodigoPostal>' . $codigoPostal . '</dte:CodigoPostal>
                                    <dte:Municipio>' . $municipio . '</dte:Municipio>
                                    <dte:Departamento>' . $depto . '</dte:Departamento>
                                    <dte:Pais>' . $pais . '</dte:Pais>
                                </dte:DireccionEmisor>
                            </dte:Emisor>
                            <dte:Receptor NombreReceptor="' . $nombreComprador . '" IDReceptor="' . $nitComprador . '" CorreoReceptor="'. $email .'" >
                                <dte:DireccionReceptor>
                                    <dte:Direccion>' . $direccionComprador . '</dte:Direccion>
                                    <dte:CodigoPostal>' . $codigoPostalComprador . '</dte:CodigoPostal>
                                    <dte:Municipio>' . $munComprador . '</dte:Municipio>
                                    <dte:Departamento>' . $deptoComprador . '</dte:Departamento>
                                    <dte:Pais>' . $paisComprador . '</dte:Pais>
                                    </dte:DireccionReceptor>
                            </dte:Receptor>
                            ';
          if($afiliacionIVA == 'PEQ'){
            $xml_data .= '<dte:Frases>
                                <dte:Frase TipoFrase="3" CodigoEscenario="1" />
                            </dte:Frases>';
          }else{
            $xml_data .= '<dte:Frases>
                                <dte:Frase TipoFrase="1" CodigoEscenario="'.$fel_datos->regimen_tributario.'" />
                            </dte:Frases>';
          }
            $xml_data .= '<dte:Items>';
            //DETALLE DE LA FACTURA  CorreoReceptor="">
            $total_impuesto = $total_amount = 0;
            foreach ($process_data["items"] as $key => $value) {
              if($afiliacionIVA !== 'PEQ'){
                $precio_total_mas_iva = $value["precio_total"]/1.12;
              }else{
                $precio_total_mas_iva = $value["precio_total"]/1.05;
              }
              
                
              
                $ln = $key+1;
                $xml_data .= '<dte:Item NumeroLinea="'.$ln.'" BienOServicio="B">
                                        <dte:Cantidad>'.$value["menuqty"].'</dte:Cantidad>
                                        <dte:UnidadMedida>CA</dte:UnidadMedida>
                                        <dte:Descripcion>'.$value["ProductName"].'</dte:Descripcion>
                                        <dte:PrecioUnitario>'.$value["precio_unidad"].'</dte:PrecioUnitario>
                                        <dte:Precio>'.$value["precio_total"].'</dte:Precio>
                                        <dte:Descuento>0</dte:Descuento>';
                //GENERA IMPUESTOS SI LA AFILIACION NO ES PEQUENO CONTRIBUYENTE
                if ($afiliacionIVA !== 'PEQ') {
                    $xml_data .= '<dte:Impuestos>
                                            <dte:Impuesto>
                                                <dte:NombreCorto>IVA</dte:NombreCorto>
                                                <dte:CodigoUnidadGravable>1</dte:CodigoUnidadGravable>
                                                <dte:MontoGravable>'.number_format($precio_total_mas_iva,2).'</dte:MontoGravable>
                                                <dte:MontoImpuesto>'.number_format(($value["precio_total"]-$precio_total_mas_iva),2).'</dte:MontoImpuesto>
                                            </dte:Impuesto>
                                        </dte:Impuestos>';
                }
                
                $xml_data .= '<dte:Total>'.$value["precio_total"].'</dte:Total>
                            </dte:Item>';
                            
                                $total_amount += $value["precio_total"];
                                $total_impuesto += number_format(($value["precio_total"]-$precio_total_mas_iva),2);    
                            
            }
                
            $xml_data .= '</dte:Items>';
            //TERMINA DETALLE DE LA FACTURA
            $xml_data .= '<dte:Totales>';
            //GENERA IMPUESTOS SI LA AFILIACION NO ES PEQUENO CONTRIBUYENTE
            if ($afiliacionIVA !== 'PEQ') {
                $xml_data .= '<dte:TotalImpuestos>
                                    <dte:TotalImpuesto NombreCorto="IVA" TotalMontoImpuesto="'.$total_impuesto.'" />
                                </dte:TotalImpuestos>';
            }
          
            $xml_data .= '<dte:GranTotal>'.$total_amount.'</dte:GranTotal>
                            </dte:Totales>';
            $xml_data .= '</dte:DatosEmision>
                    </dte:DTE>
                    <dte:Adenda>
                        <dtecomm:Informacion_COMERCIAL xmlns:dtecomm="https://www.digifact.com.gt/dtecomm" xsi:schemaLocation="https://www.digifact.com.gt/dtecomm">
                            <dtecomm:InformacionAdicional Version="7.1234654163">
                                <dtecomm:REFERENCIA_INTERNA>MGGMT0'.$company_id.$process_data["order_id"].'</dtecomm:REFERENCIA_INTERNA>
                                <dtecomm:FECHA_REFERENCIA>2020-10-16T14:05:00</dtecomm:FECHA_REFERENCIA>
                                <dtecomm:VALIDAR_REFERENCIA_INTERNA>VALIDAR</dtecomm:VALIDAR_REFERENCIA_INTERNA>
                            </dtecomm:InformacionAdicional>
                        </dtecomm:Informacion_COMERCIAL>
                    </dte:Adenda>
                </dte:SAT>
            </dte:GTDocumento>';
          	//return json_encode($xml_data);die;
            $ch = curl_init($URL);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'Authorization: ' . $token . ''));
            curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch);
            // dd("hola mundo");
            // return $output;
            $response = json_decode($output, true);
            if($company_id == 61){
                //return $response;
            }
            
            if ($response['Codigo'] === 1) {
                $save_response = new FelRespuesta();
                $save_response->comercio_id = $company_id;
                $save_response->order_id = $process_data["order_id"] ;
                $save_response->fel_serie =$response['Serie'] ;
                $save_response->fel_correlativo =$response['NUMERO'] ;
                $save_response->fel_autorizacion =$response['Autorizacion'];
                $save_response->fel_fechaEmision = $response['Fecha_DTE'];
                $save_response->save();
                $response_data = [];
                $response_data["AUTORIZACION"] = $response['Autorizacion'];
                $response_data["SERIE"] = $response['Serie'];
                $response_data["NUMERO"] = $response['NUMERO'];
                $response_data["FECHA_EMISION"] = $response['Fecha_DTE'] ;
                return $response_data;
            } else {
                mail ("rc@ie.gt", "Error para: " . $nombreEmisor , $response) ;
                $response_data['CODIGOE ERROR'] = $response['Codigo'];
                return $response_data;
            }   
        }
        
    }
    
    public  function facturabynit(Request $request){
        
        $start = microtime(true);
        mail ("rc@ie.gt", "Error para: " . $start , $request) ;
        $process_data = $request->all();
        

        //return $process_data;
        $fel_datos = FelDato::where("nit" , $process_data["comercio_nit"] )->get();

        
        if(count($fel_datos) > 0 ){
            
            $fel_datos = $fel_datos[0];
            
            $company_id = $fel_datos->comercio_id;
            $comercio_full = Comercio::find( $company_id);
        
            if(isset($process_data["ciudad"])){
                $direccionComprador = $process_data["ciudad"];
            }else{
                $direccionComprador = "Ciudad";
            }
    
          	if(isset($process_data["email"])){
                $email = $process_data["email"];
            }else{
                $email = "";
            }
            
            //$direccionComprador = ( $process_data["ciudad"] != "" ? $process_data["ciudad"] : "Ciudad" );
            //$descuento = ( $process_data["descuento"] != "" ? $process_data["ciudad"] : 0.00 );
            
            $found = FelRespuesta::where("comercio_id" , $company_id)->where("order_id", $process_data["order_id"])->get();
            if(count($found) > 0 ){
              		
                	$response_data = [];
                    $response_data["AUTORIZACION"] = $found[0]->fel_autorizacion;
                    $response_data["SERIE"] = $found[0]->fel_serie;
                    $response_data["NUMERO"] = $found[0]->fel_correlativo	;
                    $response_data["FECHA_EMISION"] = $found[0]->fel_fechaEmision ;
                    
                    $find_cliente = ClientesFel::where("comercio_id" , $company_id)->where("order_id", $process_data["order_id"])->get();
                    if(count($find_cliente) >  0 ) {
                          $response_data["nit_cliente"] = $find_cliente[0]->nit_cliente ;
                          $response_data["nombre_cliente"] = $find_cliente[0]->nombre_cliente ;
                          $response_data["direccion_cliente"] = $find_cliente[0]->direccion_cliente ;
                          $response_data["email_cliente"] = $find_cliente[0]->email_cliente ;
                    }else{
                        
                        $response_data["nit_cliente"] ="CF" ;
                        $response_data["nombre_cliente"] = "CONSUMIDOR FINAL" ;
                        $response_data["direccion_cliente"] = "CIUDAD" ;
                        $response_data["email_cliente"] ="";
                        $cliente_fel = new ClientesFel();
                            $cliente_fel->order_id = $process_data["rder_id"];
                            $cliente_fel->comercio_id = $company_id;
                            $cliente_fel->nit_cliente = "CF";
                            $cliente_fel->nombre_cliente = "CONSUMIDOR FINAL";
                            $cliente_fel->email_cliente = "";
                            $cliente_fel->direccion_cliente = "CIUDAD";
                        $cliente_fel->save();
                    }
                    return $response_data;
            }else{
                
              	if(isset($process_data["nit_cliente"] ) ){
                	$process_data["nit_cliente"] = $process_data["nit_cliente"];
                  	$process_data["nombre_cliente"] = $process_data["nombre_cliente"];
                  }else{
              		$process_data["nit_cliente"] = "CF";
                  	$process_data["nombre_cliente"] = "CONSUMIDOR FINAL";    
              	}
             	
            	$cliente_fel = new ClientesFel();
                    $cliente_fel->order_id = $process_data["order_id"];
                    $cliente_fel->comercio_id = $company_id;
                    $cliente_fel->nit_cliente = $process_data["nit_cliente"];
                    $cliente_fel->nombre_cliente = $process_data["nombre_cliente"];
                    $cliente_fel->email_cliente = $email;
                    $cliente_fel->direccion_cliente = $direccionComprador;
                $cliente_fel->save();
                // $fel_datos = FelDato::where("comercio_id" , $company_id)->get()[0];
                // dd($company_id);
                // $token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IkdULjAwMDA0NDY1Mzk0OC5MYV9MZWNoaXRhIiwibmJmIjoxNjAwODcyOTM4LCJleHAiOjE2MzE5NzY5MzgsImlhdCI6MTYwMDg3MjkzOCwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo0OTIyMCIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDkyMjAifQ.m_N2ighEylTLeCo_Ism4iJExpXghKxRetWdwVrwACt4";
                // $token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IkdULjAwMDEwMDA2NDE4My5NRU5URVNfR0VOSUEiLCJuYmYiOjE2MjQzODUyMjYsImV4cCI6MTY1NTQ4OTIyNiwiaWF0IjoxNjI0Mzg1MjI2LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjQ5MjIwIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDo0OTIyMCJ9.ATVc2D9uYOT0Oiofdbo_T0XEy8jfFyTz1P1F-lMpj10";

                $token = $fel_datos->token;
                $min_nit = $fel_datos->nit;
                $l_c = strlen ($min_nit);
                //dd($l_c);
                $txt_nit = "";
                if( $l_c < 12) {
                    $kk = 12 - $l_c;
                    for ( $i = 0; $i < $kk; $i++){
                        $txt_nit = $txt_nit . "0";
                    }
                    $full_nit = $txt_nit.$min_nit;
                }else {
                    $full_nit = $min_nit;
                }
                
                $user_name = $fel_datos->nombre_de_usuario;
                if($company_id == 61){
                    $URL = "https://felgttestaws.digifact.com.gt/gt.com.fel.api.v3/api/FELRequestV2?NIT=000044653948&TIPO=CERTIFICATE_DTE_XML_TOSIGN&FORMAT=XML&USERNAME=USUARIOTEST";
                }else{
                    $URL = "https://felgtaws.digifact.com.gt/gt.com.fel.api.v3/api/FELRequestV2?NIT=$full_nit&TIPO=CERTIFICATE_DTE_XML_TOSIGN&FORMAT=XML&USERNAME=$user_name";
                }
                
                date_default_timezone_set("America/Guatemala");
                $tipoDocumento = "FACT";
                $fechaEmision = date('Y-m-d');
                $horaEmision = date('H:m:s');
                $codigoMoneda = "GTQ";
                //DATOS DEL VENDEDOR
                $nitEmisor = $min_nit;
                $nombreEmisor = $comercio_full->name;
         
                if($company_id == 69){
                    $codEstablecimiento = "3";    
                }else if($company_id == 55){
                    $codEstablecimiento = "3";    
                }else{
                    $codEstablecimiento = "1";
                }


                $nombreComercial = $fel_datos->company_name;
                if( $fel_datos->regimen_tributario == 0 ){
                    $afiliacionIVA = "PEQ";
                  	$tipo_dte = "FPEQ";
                }else{
                    $afiliacionIVA = "GEN";    
                  	$tipo_dte = "FACT";
                }
                
                $direccionEmisor = "CIUDAD";
                $codigoPostal = "01001";
                $municipio = "GUATEMALA";
                $depto = "GUATEMALA";
                $pais = "GT";
                //DATOS DEL COMPRADOR
                
                $nombreComprador = $process_data["nombre_cliente"];
                $nitComprador = $process_data["nit_cliente"];
                
                //$direccionComprador = "CIUDAD";
                $munComprador = "GUATEMALA";
                $deptoComprador = "GUATEMALA";
                $codigoPostalComprador = "01001";
                $paisComprador = "GT";
                //GENERACION DE XML
                $xml_data = '<?xml version="1.0" encoding="UTF-8"?><dte:GTDocumento Version="0.1" xmlns:dte="http://www.sat.gob.gt/dte/fel/0.2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <dte:SAT ClaseDocumento="dte">
                        <dte:DTE ID="DatosCertificados">
                            <dte:DatosEmision ID="DatosEmision">
                                <dte:DatosGenerales Tipo="' . $tipo_dte . '" FechaHoraEmision="' . $fechaEmision . 'T' . $horaEmision . '" CodigoMoneda="' . $codigoMoneda . '" />
                                <dte:Emisor NITEmisor="' . $nitEmisor . '" NombreEmisor="' . $nombreEmisor . '" CodigoEstablecimiento="' . $codEstablecimiento . '" NombreComercial="' . $nombreComercial . '" AfiliacionIVA="' . $afiliacionIVA . '">
                                    <dte:DireccionEmisor>
                                        <dte:Direccion>' . $direccionEmisor . '</dte:Direccion>
                                        <dte:CodigoPostal>' . $codigoPostal . '</dte:CodigoPostal>
                                        <dte:Municipio>' . $municipio . '</dte:Municipio>
                                        <dte:Departamento>' . $depto . '</dte:Departamento>
                                        <dte:Pais>' . $pais . '</dte:Pais>
                                    </dte:DireccionEmisor>
                                </dte:Emisor>
                                <dte:Receptor NombreReceptor="' . $nombreComprador . '" IDReceptor="' . $nitComprador . '" CorreoReceptor="'. $email .'" >
                                    <dte:DireccionReceptor>
                                        <dte:Direccion>' . $direccionComprador . '</dte:Direccion>
                                        <dte:CodigoPostal>' . $codigoPostalComprador . '</dte:CodigoPostal>
                                        <dte:Municipio>' . $munComprador . '</dte:Municipio>
                                        <dte:Departamento>' . $deptoComprador . '</dte:Departamento>
                                        <dte:Pais>' . $paisComprador . '</dte:Pais>
                                        </dte:DireccionReceptor>
                                </dte:Receptor>
                                ';
              if($afiliacionIVA == 'PEQ'){
                $xml_data .= '<dte:Frases>
                                    <dte:Frase TipoFrase="3" CodigoEscenario="1" />
                                </dte:Frases>';
              }else{
                $xml_data .= '<dte:Frases>
                                    <dte:Frase TipoFrase="1" CodigoEscenario="'.$fel_datos->regimen_tributario.'" />
                                </dte:Frases>';
              }
                $xml_data .= '<dte:Items>';
                //DETALLE DE LA FACTURA  CorreoReceptor="">
                $total_impuesto = $total_amount = 0;
                foreach ($process_data["items"] as $key => $value) {
                  if($afiliacionIVA !== 'PEQ'){
                    $precio_total_mas_iva = $value["precio_total"]/1.12;
                  }else{
                    $precio_total_mas_iva = $value["precio_total"]/1.05;
                  }
                  
                    $ln = $key+1;
                    $xml_data .= '<dte:Item NumeroLinea="'.$ln.'" BienOServicio="B">
                                            <dte:Cantidad>'.$value["menuqty"].'</dte:Cantidad>
                                            <dte:UnidadMedida>CA</dte:UnidadMedida>
                                            <dte:Descripcion>'.$value["ProductName"].'</dte:Descripcion>
                                            <dte:PrecioUnitario>'.$value["precio_unidad"].'</dte:PrecioUnitario>
                                            <dte:Precio>'.$value["precio_total"].'</dte:Precio>
                                            <dte:Descuento>0</dte:Descuento>';
                    //GENERA IMPUESTOS SI LA AFILIACION NO ES PEQUENO CONTRIBUYENTE
                    if ($afiliacionIVA !== 'PEQ') {
                        $xml_data .= '<dte:Impuestos>
                                                <dte:Impuesto>
                                                    <dte:NombreCorto>IVA</dte:NombreCorto>
                                                    <dte:CodigoUnidadGravable>1</dte:CodigoUnidadGravable>
                                                    <dte:MontoGravable>'.number_format($precio_total_mas_iva,2).'</dte:MontoGravable>
                                                    <dte:MontoImpuesto>'.number_format(($value["precio_total"]-$precio_total_mas_iva),2).'</dte:MontoImpuesto>
                                                </dte:Impuesto>
                                            </dte:Impuestos>';
                    }
                    
                    $xml_data .= '<dte:Total>'.$value["precio_total"].'</dte:Total>
                                </dte:Item>';
                                
                                    $total_amount += $value["precio_total"];
                                    $total_impuesto += number_format(($value["precio_total"]-$precio_total_mas_iva),2);    
                                
                }
                    
                $xml_data .= '</dte:Items>';
                //TERMINA DETALLE DE LA FACTURA
                $xml_data .= '<dte:Totales>';
                //GENERA IMPUESTOS SI LA AFILIACION NO ES PEQUENO CONTRIBUYENTE
                if ($afiliacionIVA !== 'PEQ') {
                    $xml_data .= '<dte:TotalImpuestos>
                                        <dte:TotalImpuesto NombreCorto="IVA" TotalMontoImpuesto="'.$total_impuesto.'" />
                                    </dte:TotalImpuestos>';
                }
              
                $xml_data .= '<dte:GranTotal>'.$total_amount.'</dte:GranTotal>
                                </dte:Totales>';
                $xml_data .= '</dte:DatosEmision>
                        </dte:DTE>
                        <dte:Adenda>
                            <dtecomm:Informacion_COMERCIAL xmlns:dtecomm="https://www.digifact.com.gt/dtecomm" xsi:schemaLocation="https://www.digifact.com.gt/dtecomm">
                                <dtecomm:InformacionAdicional Version="7.1234654163">
                                    <dtecomm:REFERENCIA_INTERNA>MGGMT0'.$company_id.$process_data["order_id"].'</dtecomm:REFERENCIA_INTERNA>
                                    <dtecomm:FECHA_REFERENCIA>2020-10-16T14:05:00</dtecomm:FECHA_REFERENCIA>
                                    <dtecomm:VALIDAR_REFERENCIA_INTERNA>VALIDAR</dtecomm:VALIDAR_REFERENCIA_INTERNA>
                                </dtecomm:InformacionAdicional>
                            </dtecomm:Informacion_COMERCIAL>
                        </dte:Adenda>
                    </dte:SAT>
                </dte:GTDocumento>';
              	//return json_encode($xml_data);die;
                $ch = curl_init($URL);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'Authorization: ' . $token . ''));
                curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $output = curl_exec($ch);
                curl_close($ch);

                $response = json_decode($output, true);
                
                $time_elapsed_secs = microtime(true) - $start;
                //echo $time_elapsed_secs;
                
                if($company_id == 61){
                    //return $response;
                }
                
                if ($response['Codigo'] === 1) {
                    $save_response = new FelRespuesta();
                    $save_response->comercio_id = $company_id;
                    $save_response->order_id = $process_data["order_id"] ;
                    $save_response->fel_serie =$response['Serie'] ;
                    $save_response->fel_correlativo =$response['NUMERO'] ;
                    $save_response->fel_autorizacion =$response['Autorizacion'];
                    $save_response->fel_fechaEmision = $response['Fecha_DTE'];
                    $save_response->save();
                    $response_data = [];
                    $response_data["AUTORIZACION"] = $response['Autorizacion'];
                    $response_data["SERIE"] = $response['Serie'];
                    $response_data["NUMERO"] = $response['NUMERO'];
                    $response_data["FECHA_EMISION"] = $response['Fecha_DTE'] ;
                    return $response_data;
                } else {
                    $cabeceras = 'From: roberto@30y.tech' . "\r\n" .
                    'Reply-To: roberto@30y.tech' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
                    mail ("rc@ie.gt", "Error para: " . $nombreEmisor , $response ,$cabeceras) ;
                    $response_data['CODIGOE ERROR'] = $response;
                    return $response_data;
                }   
            }
        }else{
            $cabeceras = 'From: roberto@30y.tech' . "\r\n" .
                    'Reply-To: roberto@30y.tech' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
                    mail ("rc@ie.gt", "Error para: " . json_encode( $process_data ) , "No hay datos FEL para el comercio" ,$cabeceras) ;
            return ["error_no"=>1,"error_message" => "No hay datos FEL para el comercio"];
        }
        
        
        
    }
    
}
