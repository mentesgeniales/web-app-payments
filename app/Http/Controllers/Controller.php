<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $client_id ;
    public function curl ( $url , $data = null , $xml = false , $token = ""){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        if($xml == true){
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'Authorization: ' . $token . ''));
        }else{
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        }
        if($data != null && $xml == false) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }else{
            curl_setopt($ch, CURLOPT_POSTFIELDS, "$data");
        }
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $remote_server_output = curl_exec ($ch);
        curl_close ($ch);
        return json_decode ($remote_server_output , true);
    }

    public function get_the_user_ip() {
        if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function clean_client_id($client_id){
        $this->client_id = str_replace( "GT2020100" , "", $client_id);
        $this->client_id = (int) $this->comercio_id;
        $this->client_id = Comercio::find( $this->client_id);
    }

    public function log_curl(){
        
    }
    
    
    
}
