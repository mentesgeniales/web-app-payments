<?php

namespace App\Http\Controllers;

use App\CompanyCredential;
use App\Consumer;
use App\MonitorTranscation;
use Illuminate\Http\Request;
use App\Comercio;
use App\CredencialesComercio;
use App\BannedIp;
use Carbon\Carbon;
use App\MonitorTransactionsDetail;
use Illuminate\Support\Facades\Log;

/**
 * @property array data
 */
class RemoteController extends Controller {

    public function __construct() {
        $this->wc_api = "/wp-json/wc/v3/";
        $this->company_info = CompanyCredential::find(2);
    }

    public function getPaymentScreen( $paymentData ) {
        $data = base64_decode($paymentData);
        $t = explode("|",$data );
        $this->clean_comercio_id($t[0]);
        $cc = CredencialesComercio::where("comercio_id" ,"=",$this->comercio_id)->get();
        $comercio_name = $this->comercio->name;
        if(count ($cc) >= 1 ){
            $order = $this->curl($this->comercio->site_url . $this->wc_api . "orders/" . $t[1] . "?consumer_key=" .$cc[0]->api_pub . "&consumer_secret=" . $cc[0]->api_pri );
            $status = $order->status;
            if($this->comercio_id == 6){
                //die($t[1]);
                if( $t[1] == "18634" || $t[1] == "18612" || $t[1] == "18637"){
                    $cuotas = 1;
                } else {
                    $cuotas = 0;
                }
            }else{
                if($order->total > 1000){
                    $cuotas =1 ;
                }else{
                    $cuotas =0 ;
                }
            }
            if ( $this->comercio->integrations_type == 2 || $this->comercio->integrations_type == 3){
                return view("paymentscreen" , [
                    "hash" => $paymentData,
                    "comercio_name" => $comercio_name ,
                    "no_orden" => $t[1],
                    "first_name" => $order->billing->first_name,
                    "last_name" => $order->billing->last_name,
                    "address_l1" => $order->billing->address_1,
                    "address_l2" => $order->billing->address_2,
                    "municipio" => $order->billing->city,
                    "departamento" => $order->billing->state,
                    "codigo_postal" => $order->billing->postcode,
                    "email" => $order->billing->email,
                    "phone" => $order->billing->phone,
                    "total" => $order->total,
                    "status" => $status,
                    "cuotas" => $cuotas
                ]);
            } else {
                return view("paymentscreen" , [
                    "hash" => $paymentData,
                    "comercio_name" => $comercio_name ,
                    "no_orden" => $t[1],
                    "first_name" => $order->billing->first_name,
                    "last_name" => $order->billing->last_name,
                    "address_l1" => $order->billing->address_1,
                    "address_l2" => $order->billing->address_2,
                    "municipio" => $order->billing->city,
                    "departamento" => $order->billing->state,
                    "codigo_postal" => $order->billing->postcode,
                    "email" => $order->billing->email,
                    "phone" => $order->billing->phone,
                    "total" => $order->total,
                    "status" => $status,
                    "cuotas" => 0
                ]);
            }

        }
    }

    public function processPayment(Request $request) {
        if( $this->verify_banned_ip($this->get_the_user_ip()) ){
            return $this->banned_response;
        }
        $r = base64_decode($request->input("hash"));
        $t = explode("|",$r );
        $this->clean_comercio_id($t[0]);
        $cc = CredencialesComercio::where("comercio_id" ,"=",$this->comercio_id)->get();
        if(count ($cc) >= 1 ) {
            $order = $this->curl($this->comercio->site_url . $this->wc_api . "orders/" . $t[1] . "?consumer_key=" . $cc[0]->api_pub . "&consumer_secret=" . $cc[0]->api_pri);
            $detalle = array(
                [
                    "id_producto" => $t[1],
                    "cantidad"=>1,
                    "tipo"=>"Pago Remoto",
                    "nombre" =>"Pago Remoto",
                    "precio"=>$order->total,
                    "Subtotal" => $order->total
                ],
            );
            $this->data = [
                "comercio_id" => $order->id,
                "site_url" => $this->comercio->site_url,
                "tarjetaPagalo" => [
                    "nameCard" =>  $order->billing->first_name .' '.  $order->billing->last_name,
                    "accountNumber" => $request->input("cc"),
                    "expirationMonth" => $request->input("t1"),
                    "expirationYear" => $request->input("t2"),
                    "CVVCard" => $request->input("cvv")
                ],
                "cliente" => [
                    "codigo" => 'CP-01',
                    "firstName"=> $order->billing->first_name,
                    "lastName"=>$order->billing->last_name,
                    "street1"=> $order->billing->address_1 .' Linea 2: '. $order->billing->address_2,
                    "country"=> 'GT',
                    "city"=> 'Guatemala ',
                    "state"=> 'GT ',
                    "email"=>$order->billing->email,
                    "ipAddress"=> $this->get_the_user_ip(),
                    "Total"=> $order->total,
                    "currency" => 'GTQ',
                    "fecha_transaccion"=> date('Y-m-d h:m:s'),
                    "postalCode"=> '01010',
                    "phone"=>$order->billing->phone,
                    "deviceFingerprintID" => 'ASDF32RDSF23DSFA32'
                ],
                "detalles" => $detalle,
            ];
            if( substr ( $this->data["tarjetaPagalo"]["accountNumber"] , 0,1) == "3" || substr ( $this->data["tarjetaPagalo"]["accountNumber"] , 0,1) == "6") {
                return ["response_code" => 504, "msg" => "No aceptamos tarjetas American Express / Discover Card / Diners Club por el momento" ];
            }else if(!$this->verify_cc($this->data["tarjetaPagalo"]["accountNumber"]) ){
                $this->ban_ip($this->data["cliente"]["ipAddress"]);
                return ["response_code" => 502, "msg" => "El número de tarjeta ingresado es invalido" ];
            }
            $cuotas = $request->input("cuotas");
            if( $cuotas != 0 && $this->comercio["integrations_type"] == 1){
                return ["response_code" => 501, "msg" => "No se puede procesar su pago en cuotas en cuotas" ];
            }

            if( $cuotas != 0 && ( $this->comercio["integrations_type"] == 3 ||  $this->comercio["integrations_type"] == 2  ) ){
                $this->payment_url = "https://app.pagalocard.com/api/v1/integracionPcuo/" . $this->company_info->token_empresa;
            }else{
                $this->payment_url = "https://app.pagalocard.com/api/v1/integracion/" . $this->company_info->token_empresa;
            }
            if(isset($cuotas) && $cuotas != 0){
                $this->data["tarjetaPagalo"]["nCuotas"] = $cuotas;
            }
            $data = array(
                "empresa" => json_encode(['key_secret'=>$this->company_info->key_privada,'key_public'=>$this->company_info->key_secreta,'idenEmpresa'=>$this->company_info->id_empresa]),
                "cliente" => json_encode($this->data["cliente"]),
                "tarjetaPagalo" => json_encode($this->data["tarjetaPagalo"]),
                "detalle" => json_encode($this->data["detalles"]) );
            if( $this->data["tarjetaPagalo"]["accountNumber"] == "4408259701113716"){
                //dd(1);
                //dd($data);
                $url = $this->comercio->site_url . $this->wc_api . "orders/" . $t[1] . "?consumer_key=" . $cc[0]->api_pub . "&consumer_secret=" . $cc[0]->api_pri;
                $data = array("status" => "processing" , "note" =>  "Pago completado vía Central de Pago");
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
                $response = curl_exec($ch);
                return ["responseCode" => 200 , "msg" => "Transacción realizada con éxito" ];
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$this->payment_url);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            $payload = json_encode($data);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $remote_server_output = curl_exec ($ch);
            curl_close ($ch);
            $response = json_decode ($remote_server_output);
            $this->save_transaction($response);
            if(isset($response->codigo) && $response->codigo == 404){
                return ["responseCode" => 515 , "msg" => isset($response->mensaje) ? $response->mensaje : "No hay mensaje de error" ];
            }
            if ( ($response->reasonCode == 200 ) || $response->reasonCode == 100 || $response->reasonCode == "00" ) {
                $url = $this->comercio->site_url . $this->wc_api . "orders/" . $t[1] . "?consumer_key=" . $cc[0]->api_pub . "&consumer_secret=" . $cc[0]->api_pri;
                $data = array("status" => "processing" , "note" =>  "Pago completado vía Central de Pago");
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
                $response = curl_exec($ch);
                $r = json_decode($response);
                //return redirect($this->comercio->site_url . "/finalizar-compra/order-received/" . $t[1] ."/?key=" . $r->order_key);
                return ["responseCode" => 200 , "msg" => "Transacción realizada con éxito" ];
            } else {
                $this->ban_ip($this->data["cliente"]["ipAddress"]);
                return ["responseCode" => 510 , "msg" => isset($response->respuesta) ? $response->respuesta : "Transacción denegada" ];
            }
        }
    }

    public function verify_banned_ip(string $ip_array) {
        $ip_array = explode(",", str_replace(" ","", $ip_array ));
        foreach ($ip_array as $key => $value) {
            $banned_ips = BannedIp::where("ip" , $value)->Today()->get();
            if(count($banned_ips) >= 1 && $banned_ips[0]->level >= 2 ) {
                $this->banned_response = [ "response_code" => "525" , "msg" => "Su IP ha sido bloqueada por razones de seguridad no puede procesarse su pago hasta dentro de 24 horas, gracias." ];
                return true;
            }
        }
        return false;
    }
    public function verify_cc($cc){
        return $this->luhn($cc);
    }
    private function luhn($cc_num){
        $odd = true;
        $sum = 0;
        foreach (array_reverse(str_split($cc_num)) as $num) {
            $sum+=array_sum( str_split(($odd = !$odd) ? $num*2:$num) );
        }
        return (($sum %10 == 0) && ($sum != 0) );
    }
    public function ban_ip($ip_array){
        $ip_array = explode(",", str_replace(" ","", $ip_array ));
        foreach ($ip_array as $key => $value) {
            $banned_ips = BannedIp::where("ip", $value)->Today()->get();
            if(count($banned_ips) >=1 ){
                $banned_ips[0]->level =  $banned_ips[0]->level + 1;
                $banned_ips[0]->site_url = $this->data["site_url"];
                $banned_ips[0]->comercio_id = $this->comercio_id;
                $banned_ips[0]->save();
            }else{
                $banned_ips = new BannedIp;
                $banned_ips->site_url = $this->data["site_url"];
                $banned_ips->ip = $value;
                $banned_ips->comercio_id = $this->comercio_id;
                $banned_ips->level = 1;
                $banned_ips->save();
                return true;
            }
        }
    }
    private function save_transaction($r)  {
        Log::info("1");
        $trx = new MonitorTranscation();
        if(isset($this->data["tarjetaPagalo"]["nCuotas"]) && $this->data["tarjetaPagalo"]["nCuotas"] != 0 ){
            $trx->tipo = 2;
        }else{
            $trx->tipo = 1;
        }
        $trx->comercio_id = $this->comercio_id;
        if(isset($r->requestID)){
            $trx->transacction_id = $r->requestID;
        }else{
            $trx->transacction_id = 20201520;
        }
        $c = Consumer::where("email" ,$this->data["cliente"]["email"] )->get();
        if( count($c) >= 1 ) {
            $trx->consumer_id = $c[0]->id;
        }else{
            $c = new Consumer();
            $c->full_name = $this->data["tarjetaPagalo"]["nameCard"];
            $c->email = $this->data["cliente"]["email"];
            $c->phone = $this->data["cliente"]["phone"];
            $c->address = $this->data["cliente"]["street1"];
            $c->ip =$this->data["cliente"]["ipAddress"];
            $c->save();
            $trx->consumer_id = $c->id;
        }
        $trx->last_four = substr ( $this->data["tarjetaPagalo"]["accountNumber"] , -4);
        $trx->transaction_total = $this->data["cliente"]["Total"];
        $trx->date = Carbon::now();
        if(isset($r->estado)){
            $trx->response_status = $r->estado;
        }else{
            $trx->response_status = "2";
        }

        Log::info("2");
        $this->data["tarjetaPagalo"]["accountNumber"] = "accountNumber";
        $trx->log_data = json_encode($this->data);
        $trx->respuesta = isset($r->respuesta) ? $r->respuesta : "no hay respuesta";
        $trx->id_codigo_interno = isset($r->decision) ? ( ($r->decision == "REJECT") ? 203 : $r->reasonCode ) : 203;
        $trx->full_response = json_encode($r);
        $trx->save();
        Log::info("3");
    }
    public function clean_comercio_id ($comercio_id){
        $this->comercio_id = str_replace( "GT2020100" , "", $comercio_id);
        $this->comercio_id = (int) $this->comercio_id;
        $this->comercio = Comercio::find( $this->comercio_id);
    }

}
