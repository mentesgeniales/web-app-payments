<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Departamento;
use App\Municipio;
use App\Poblado;
use SoapClient;
use SoapHeader;
use SimpleXMLElement;
use XmlParser;
class SoapController extends Controller {
    
    public function __construct( $env , $u , $p , $c ){
        if($env == 'prod' ){
            $this->user = $u;
            $this->password = $p;
            $this->codigo_credito = $c;
            $this->wsdl = "http://ws.caexlogistics.com:8080/wsCAEXLogisticsSB/wsCAEXLogisticsSB.asmx?wsdl";
            //$this->wsdl = "http://wsqa.caexlogistics.com:1880/wsCAEXLogisticsSB/wsCAEXLogisticsSB.asmx";
        }else{
            $this->user = $u;
            $this->password = $p;
            $this->codigo_credito = $c;
            //$this->wsdl = "http://ws.caexlogistics.com:8080/wsCAEXLogisticsSB/wsCAEXLogisticsSB.asmx?wsdl";
            $this->wsdl = "http://wsqa.caexlogistics.com:1880/wsCAEXLogisticsSB/wsCAEXLogisticsSB.asmx";
        }
    }

    /**
     * @return mixed
     */
    public function getDepartamentos(){
        Departamento::truncate();
        $body = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                        <soap:Body>
                            <ObtenerListadoDepartamentos xmlns="http://www.caexlogistics.com/ServiceBus">
                            <Autenticacion>
                                <Login>'.$this->user .'</Login>
                                <Password>'.$this->password .'</Password>
                            </Autenticacion>
                            </ObtenerListadoDepartamentos>
                        </soap:Body>
                    </soap:Envelope>';
        $data = $this->curl($body);
        $v = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $data);
        $xml = new SimpleXMLElement($v);
        $body = $xml->soapBody->ObtenerListadoDepartamentosResponse->ResultadoObtenerDepartamentos->ListadoDepartamentos;
        $departamentos = json_decode(json_encode((array)$body), TRUE);
        foreach ($departamentos as $key => $value) {
            foreach ($value as $key_dep => $value_dep) {
                $departamento = new Departamento;
                $departamento->codigo = $value_dep["Codigo"];
                $departamento->name = $value_dep["Nombre"];
                $departamento->save();
            }
        }
        return ["" => "", "lista_departamentos" => Departamento::all() ];
    }

    /**
     * @return mixed
     */
    public function getMunicipios(){
        Municipio::truncate();
        $body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://www.caexlogistics.com/ServiceBus">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <ser:ObtenerListadoMunicipios>
                            <!--Optional:-->
                            <ser:Autenticacion>
                                <!--Optional:-->
                                <ser:Login>'.$this->user .'</ser:Login>
                                <!--Optional:-->
                                <ser:Password>'.$this->password.'</ser:Password>
                            </ser:Autenticacion>
                        </ser:ObtenerListadoMunicipios>
                    </soapenv:Body>
                </soapenv:Envelope>'; /// Your SOAP XML needs to be in this variable

        $data = $this->curl($body);
        $v = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $data);
        $xml = new SimpleXMLElement($v);
        $body = $xml->soapBody->ObtenerListadoMunicipiosResponse->ResultadoObtenerMunicipios->ListadoMunicipios;
        $municipios = json_decode(json_encode((array)$body), TRUE);
        foreach ($municipios as $key => $value) {
            foreach ($value as $key_dep => $value_mun) {
                $municipios = new Municipio;
                $municipios->codigo = $value_mun["Codigo"];
                $municipios->name = $value_mun["Nombre"];
                $municipios->CodigoDepto = $value_mun["CodigoDepto"];
                $municipios->CodigoCabecera = $value_mun["CodigoCabecera"];
                $municipios->save();
            }
        }
        return ["" => "", "lista_municipios" => Municipio::all() ];
        //return response()->success( [ "lista_municipios" => Municipio::all() ] );
    }

    /**
     * @return mixed
     */
    public function getPoblados(){
        poblados::truncate();
        $body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://www.caexlogistics.com/ServiceBus">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <ser:ObtenerListadoPoblados>
                            <!--Optional:-->
                            <ser:Autenticacion>
                                <!--Optional:-->
                                <ser:Login>'.$this->user .'</ser:Login>
                                <!--Optional:-->
                                <ser:Password>'.$this->password.'</ser:Password>
                            </ser:Autenticacion>
                        </ser:ObtenerListadoPoblados>
                    </soapenv:Body>
                </soapenv:Envelope>'; /// Your SOAP XML needs to be in this variable

        $data = $this->curl($body);
        $v = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $data);
        $xml = new SimpleXMLElement($v);
        $body = $xml->soapBody->ObtenerListadoPobladosResponse->ResultadoObtenerPoblados->ListadoPoblados;
        $poblados = json_decode(json_encode((array)$body), TRUE);
        foreach ($poblados as $key => $value) {
            foreach ($value as $key_dep => $value_pob) {
                $poblados = new poblados;
                $poblados->codigo = $value_pob["Codigo"];
                $poblados->name = $value_pob["Nombre"];
                $poblados->CodigoDepto = $value_pob["CodigoDepto"];
                $poblados->CodigoMunicipio = $value_pob["CodigoMunicipio"];
                $poblados->save();
            }
        }
        return ["" => "", "lista_poblados" => Poblado::all() ];
        //return response()->success( [ "lista_poblados" => Poblado::all() ] );
    }

    /**
     * @param $cliente
     * @return mixed
     */
    public function registrarGuia($cliente){
        //dd($cliente["total"]);
        //dd($this->wsdl);
        $body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://www.caexlogistics.com/ServiceBus">
                 <soapenv:Header/>
                 <soapenv:Body>
                    <ser:GenerarGuia>
                       <!--Optional:-->
                       <ser:Autenticacion>
                          <!--Optional:-->
                          <ser:Login>'.$this->user.'</ser:Login>
                          <!--Optional:-->
                          <ser:Password>'.$this->password.'</ser:Password>
                       </ser:Autenticacion>
                       <!--Optional:-->
                       <ser:ListaRecolecciones>
                          <!--Zero or more repetitions:-->
                          <ser:DatosRecoleccion>
                             <!--Optional:-->
                             <ser:RecoleccionID>'.$cliente["order_id"].'</ser:RecoleccionID>
                             <!--Optional:-->
                             <ser:RemitenteNombre>'.$cliente["comercio_name"].' </ser:RemitenteNombre>
                             <!--Optional:-->
                             <ser:RemitenteDireccion>GUATEMALA</ser:RemitenteDireccion>
                             <!--Optional:-->
                             <ser:RemitenteTelefono>22345943</ser:RemitenteTelefono>
                             <!--Optional:-->
                             <ser:DestinatarioNombre>'.$cliente["name"].'</ser:DestinatarioNombre>
                             <!--Optional:-->
                             <ser:DestinatarioDireccion>'.$cliente["address"].'</ser:DestinatarioDireccion>
                             <!--Optional:-->
                             <ser:DestinatarioTelefono>'.$cliente["phone_number"].'</ser:DestinatarioTelefono>
                             <ser:DestinatarioContacto>'.$cliente["name"].'</ser:DestinatarioContacto>
                             <!--Optional:-->
                             <ser:DestinatarioNIT>CF</ser:DestinatarioNIT>
                             <!--Optional:-->
                             <ser:ReferenciaCliente1>'.$cliente["productos"].'</ser:ReferenciaCliente1>
                             <!--Optional:-->
                             <ser:ReferenciaCliente2>'.$cliente["productos2"].'</ser:ReferenciaCliente2>
                             <!--Optional:-->
                             <ser:CodigoPobladoDestino>'.$cliente["poblado"].'</ser:CodigoPobladoDestino>
                             <!--Optional:-->
                             <ser:CodigoPobladoOrigen>2350</ser:CodigoPobladoOrigen>
                             <!--Optional:-->
                             <ser:TipoServicio>'.$cliente["tipo_guia"].'</ser:TipoServicio>
                             <ser:MontoCOD>0</ser:MontoCOD>
                             <!--Optional:-->
                             <ser:FormatoImpresion>1</ser:FormatoImpresion>
                             <ser:CodigoCredito>'.$this->codigo_credito.'</ser:CodigoCredito>
                             <ser:MontoAsegurado>'.$cliente["total"].'</ser:MontoAsegurado>
                             <!--Optional:-->
                             <ser:Observaciones>NA</ser:Observaciones>
                             <!--Optional:-->
                             <ser:Piezas>
                                <!--Zero or more repetitions:-->
                                <ser:Pieza>
                                   <ser:NumeroPieza>1</ser:NumeroPieza>
                                   <!--Optional:-->
                                   <ser:TipoPieza>1</ser:TipoPieza>
                                   <ser:PesoPieza>1</ser:PesoPieza>
                                   <ser:MontoCOD>'.$cliente["total"].'</ser:MontoCOD>
                                </ser:Pieza>
                             </ser:Piezas>
                          </ser:DatosRecoleccion>
                       </ser:ListaRecolecciones>
                    </ser:GenerarGuia>
                 </soapenv:Body>
              </soapenv:Envelope>'; /// Your SOAP XML needs to be in this variable
        //dd($body);
        $data = $this->curl($body);
        $v = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $data);
        $xml = new SimpleXMLElement($v);
        $body = $xml->soapBody->GenerarGuiaResponse;
        $resExitoso = (array)  $body->ResultadoGenerarGuia->ResultadoOperacionMultiple->ResultadoExitoso;
        //dd($data);
        //dd($xml->soapBody->GenerarGuiaResponse->ResultadoGenerarGuia->ResultadoOperacionMultiple->ResultadoExitoso);
        if( $resExitoso[0] == "true" ){
            $datos["ResultadoExitoso"] = true;
            $a = (array) $body->ResultadoGenerarGuia->ListaRecolecciones->DatosRecoleccion->URLConsulta;
            $datos["link"] = $a[0];
            $b = (array) $body->ResultadoGenerarGuia->ListaRecolecciones->DatosRecoleccion->NumeroGuia;
            $datos["no_guia"] = $b[0];
        } else {
            $e = (array)  $body->ResultadoGenerarGuia->ResultadoOperacionMultiple->MensajeError;
            $datos["ResultadoExitoso"] = false;
            $datos["error"] = $e[0];
            $y = (array) $body->ResultadoGenerarGuia->ListaRecolecciones->DatosRecoleccion->ResultadoOperacion->MensajeError;
            $datos["subError"] = $y[0];
        }
        return $datos;
    }

    public function getManifiesto($di , $de){
        $body = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ser="http://www.caexlogistics.com/ServiceBus">
                   <soap:Header/>
                   <soap:Body>
                      <ser:ObtenerUrlManifiesto>
                         <!--Optional:-->
                         <ser:Autenticacion>
                            <!--Optional:-->
                            <ser:Login>'.$this->user.'</ser:Login>
                            <!--Optional:-->
                            <ser:Password>'.$this->password.'</ser:Password>
                         </ser:Autenticacion>
                         <ser:TipoManifiesto>2</ser:TipoManifiesto>
                         <ser:FechaInicial>'.$di.'</ser:FechaInicial>
                         <ser:FechaFinal>'.$de.'</ser:FechaFinal>
                         <!--Optional:-->
                         <ser:CodigoCredito>'.$this->codigo_credito.'</ser:CodigoCredito>
                         <!--Optional:-->
                         <ser:TipoServicio>1</ser:TipoServicio>
                      </ser:ObtenerUrlManifiesto>
                   </soap:Body>
                </soap:Envelope>'; /// Your SOAP XML needs to be in this variable
        $data = $this->curl($body);
        //dd($body);
        $v = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $data);
        $xml = new SimpleXMLElement($v);
        $body = $xml->soapBody->ObtenerUrlManifiestoResponse ;
        $resExitoso = (array)  $body->ResultadoObtenerUrlManifiesto->ResultadoOperacion->ResultadoExitoso;
        if( $resExitoso[0] == "true" ){
            $datos["ResultadoExitoso"] = true;
            $a = (array) $body->ResultadoObtenerUrlManifiesto->UrlManifiesto;
            $datos["link"] = $a[0];
        } else {
            $e = (array)  $body->ResultadoObtenerUrlManifiesto->ResultadoOperacion->MensajeError;
            $datos["ResultadoExitoso"] = false;
            $datos["error"] = $e[0];
            $datos["subError"] = $y[0];
        }
        return $datos;
    }

    public function curl($body, $data = null){
        $headers = array(
            'Content-Type: text/xml; charset="utf-8"',
            'Content-Length: '.strlen($body),
            'Accept: text/xml',
            'Cache-Control: no-cache',
            'Pragma: no-cache',
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $this->wsdl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        return curl_exec($ch);
    }

}
