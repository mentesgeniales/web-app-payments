<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FelDato;
use App\Comercio;
use App\FelRespuesta;
use App\CertificadorRespuesta;
class FelController extends Controller {

    public function __construct () {
        $nit = request("nit");
        $nombre_cliente = request("nombre_cliente");
        $nit_cliente = request("nit_cliente");
        $this->fel = FelDato::where("nit" , $nit )->first();
        $this->fel->comercio;
        $this->fel->regimen;
        $this->data = (object) [
            "order_id"  => request("order_id") ,
            "nit"       => str_replace("-","",$nit) ,
            "datos_fel" => $this->fel,
            "items"     => request("items"),
            "nombre_cliente" => (isset($nombre_cliente) ? $nombre_cliente : "CONSUMIDOR FINAL") ,
            "nit_cliente"  => (isset($nit_cliente) ? $nit_cliente : "CF") ,
            "email_cliente"  => (isset($email_cliente) ? $email_cliente : "") ,
            "address_cliente"  => (isset($email_cliente) ? $email_cliente : "CIUDAD") ,
        ];            
    }

    public function certificar() {
        
        if ( $this->data->nit == "" ){
            logger("El nit no era valido");
            return json_encode( ["error_code" => 204 , "mensaje" => "Usuario no registrado para FEL" ] );
        }else{
            logger($this->data->nit);    
        }        
        
        $verifica_fel = $this->get_cert_response();
        if( $verifica_fel != null ) {
            return json_encode( array_change_key_case ( $verifica_fel->toArray() , CASE_UPPER ) );
        } else {
            $this->set_sat_xml();

            $respuesta_fel = array_change_key_case ($this->process_cert () , CASE_LOWER ) ;
            
            if($this->data->datos_fel->comercio_id == 74  ){
                // 
                // dd( $this->process_cert () );
            }
            logger($respuesta_fel["codigo"]);
            if($respuesta_fel["codigo"] == 9022){
                
                $respuesta_fel = FelRespuesta::select("fel_serie as serie", "fel_autorizacion as autorizacion", "fel_fechaEmision as fecha_dte", "fel_correlativo as numero")->where("order_id" , $this->data->order_id)->where("comercio_id" , $this->data->datos_fel->comercio_id)->first();
                $respuesta_fel = $respuesta_fel->toArray();
                $respuesta_fel += ["nombre_comprador" => "Consumidor Final" , "nit_comprador" => "CF"];
                $save_cert_datos = new CertificadorRespuesta;
                $save_cert_datos->fill($respuesta_fel);
                $save_cert_datos->save();
                return json_encode (array_change_key_case ( $respuesta_fel , CASE_UPPER) );
            } else if ($respuesta_fel["codigo"] == 1) {
                $respuesta_fel += ["order_id" => $this->data->order_id , "dte_leyenda" => $this->data->datos_fel->regimen->xml_frase_type];
                $save_cert_datos = new CertificadorRespuesta;
                $save_cert_datos->fill($respuesta_fel);
                $save_cert_datos->save();
                logger("CertCompletada");
                return json_encode(array_change_key_case ( $this->get_cert_response($save_cert_datos->id)->toArray(), CASE_UPPER ) );
            }else{
                return json_encode( ["error_code" => $respuesta_fel["codigo"] , "mensaje" => $respuesta_fel["mensaje"] ] );
            }
        }
    }
    
    private function get_cert_response($id = null) {
        if($id != null){
            return CertificadorRespuesta::select("autorizacion", "serie" , "numero","fecha_dte", "nit_comprador", "nombre_comprador" , "dte_leyenda")->find($id);
        }else{
            return CertificadorRespuesta::select("autorizacion", "serie" , "numero","fecha_dte", "nit_comprador", "nombre_comprador" , "dte_leyenda")->where("nit_eface" , $this->prepare_nit() )->where("order_id", $this->data->order_id)->where("codigo" , 1)->first();
        }
    }
    
    private function process_cert() {
        if ( ($this->data->datos_fel->comercio_id == 61 ) ){
            $URL = "https://felgttestaws.digifact.com.gt/gt.com.fel.api.v3/api/FELRequestV2?NIT=".$this->data->nit."&TIPO=CERTIFICATE_DTE_XML_TOSIGN&FORMAT=XML&USERNAME=".$this->data->datos_fel->nombre_de_usuario;
        }else{
            $URL = "https://felgtaws.digifact.com.gt/gt.com.fel.api.v3/api/FELRequestV2?NIT=".$this->data->nit."&TIPO=CERTIFICATE_DTE_XML_TOSIGN&FORMAT=XML&USERNAME=".$this->data->datos_fel->nombre_de_usuario;
        }
        return $this->curl ( $URL , $this->data->xml_data , true , $this->data->datos_fel->token );
    }

    private function set_sat_xml () {
        $fechaEmision = date('Y-m-d');
        $horaEmision = date('H:m:s');
        $codigoMoneda = "GTQ";
        $xml_data = '<?xml version="1.0" encoding="UTF-8"?><dte:GTDocumento Version="0.1" xmlns:dte="http://www.sat.gob.gt/dte/fel/0.2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <dte:SAT ClaseDocumento="dte">
                    <dte:DTE ID="DatosCertificados">
                        <dte:DatosEmision ID="DatosEmision">
                            <dte:DatosGenerales Tipo="' . $this->data->datos_fel->regimen->xml_dte_type . '" FechaHoraEmision="' . $fechaEmision . 'T' . $horaEmision . '" CodigoMoneda="' . $codigoMoneda . '" />
                            <dte:Emisor NITEmisor="' . $this->data->datos_fel->nit . '" NombreEmisor="' . $this->data->datos_fel->comercio->name . '" CodigoEstablecimiento="' . $this->data->datos_fel->no_establecimiento . '" NombreComercial="' . $this->data->datos_fel->company_name . '" AfiliacionIVA="' . $this->data->datos_fel->regimen->xml_affiliation_code . '">
                                <dte:DireccionEmisor>
                                    <dte:Direccion>' . $this->data->datos_fel->comercio->address . '</dte:Direccion>
                                    <dte:CodigoPostal>01001</dte:CodigoPostal>
                                    <dte:Municipio>GUATEMALA</dte:Municipio>
                                    <dte:Departamento>GUATEMALA</dte:Departamento>
                                    <dte:Pais>GT</dte:Pais>
                                </dte:DireccionEmisor>
                            </dte:Emisor>
                            <dte:Receptor NombreReceptor="' . $this->data->nombre_cliente. '" IDReceptor="' .  $this->data->nit_cliente . '" CorreoReceptor="'. $this->data->email_cliente .'" >
                                <dte:DireccionReceptor>
                                    <dte:Direccion>' . $this->data->address_cliente . '</dte:Direccion>
                                    <dte:CodigoPostal>01001</dte:CodigoPostal>
                                    <dte:Municipio>GUATEMALA</dte:Municipio>
                                    <dte:Departamento>GUATEMALA</dte:Departamento>
                                    <dte:Pais>GT</dte:Pais>
                                    </dte:DireccionReceptor>
                            </dte:Receptor>
                            <dte:Frases>
                                <dte:Frase TipoFrase="'.$this->data->datos_fel->regimen->xml_frase_scenario.'" CodigoEscenario="'.$this->data->datos_fel->regimen->xml_type_scenario.'" />';
                                if( ( $this->fel->nit == "59846801" )  ) {
                                    $xml_data.='<dte:Frase TipoFrase="2" CodigoEscenario="1" />';
                                }
                $xml_data.='</dte:Frases>';
                $xml_data.='<dte:Items>';
                $total_impuesto = $total_amount = 0;
                foreach ($this->data->items as $key => $value) {
                    if($this->data->datos_fel->regimen->xml_affiliation_code !== 'PEQ'){
                        $precio_total_mas_iva = $value["precio_total"]/1.12;
                    }else{
                        $precio_total_mas_iva = $value["precio_total"]/1.05;
                    }
                    $ln = $key+1;
                    $xml_data.= '<dte:Item NumeroLinea="'.$ln.'" BienOServicio="B">
                                            <dte:Cantidad>'.$value["menuqty"].'</dte:Cantidad>
                                            <dte:UnidadMedida>CA</dte:UnidadMedida>
                                            <dte:Descripcion>'.$value["ProductName"].'</dte:Descripcion>
                                            <dte:PrecioUnitario>'.$value["precio_unidad"].'</dte:PrecioUnitario>
                                            <dte:Precio>'.$value["precio_total"].'</dte:Precio>
                                            <dte:Descuento>0</dte:Descuento>';
                    if ($this->data->datos_fel->regimen->xml_affiliation_code !== 'PEQ') {
                        $xml_data.= '<dte:Impuestos>
                                        <dte:Impuesto>
                                            <dte:NombreCorto>IVA</dte:NombreCorto>
                                            <dte:CodigoUnidadGravable>1</dte:CodigoUnidadGravable>
                                            <dte:MontoGravable>'.number_format($precio_total_mas_iva,2).'</dte:MontoGravable>
                                            <dte:MontoImpuesto>'.number_format(($value["precio_total"]-$precio_total_mas_iva),2).'</dte:MontoImpuesto>
                                        </dte:Impuesto>
                                    </dte:Impuestos>';
                    }
                        $xml_data.='<dte:Total>'.$value["precio_total"].'</dte:Total>
                                </dte:Item>';
                    $total_amount += $value["precio_total"];
                    $total_impuesto += number_format(($value["precio_total"]-$precio_total_mas_iva),2);
                }
                $xml_data.='</dte:Items>';
                $xml_data.='<dte:Totales>';
                if ($this->data->datos_fel->regimen->xml_affiliation_code !== 'PEQ') {
                    $xml_data.='<dte:TotalImpuestos>
                                    <dte:TotalImpuesto NombreCorto="IVA" TotalMontoImpuesto="'.$total_impuesto.'" />
                                </dte:TotalImpuestos>';
                }
                $xml_data.=     '<dte:GranTotal>'.$total_amount.'</dte:GranTotal>
                            </dte:Totales>';
            $xml_data.='</dte:DatosEmision>
                    </dte:DTE>
                    <dte:Adenda>
                        <dtecomm:Informacion_COMERCIAL xmlns:dtecomm="https://www.digifact.com.gt/dtecomm" xsi:schemaLocation="https://www.digifact.com.gt/dtecomm">
                            <dtecomm:InformacionAdicional Version="7.1234654163">
                                <dtecomm:REFERENCIA_INTERNA>MGGMT0'.$this->data->datos_fel->comercio_id.$this->data->order_id.'</dtecomm:REFERENCIA_INTERNA>
                                <dtecomm:FECHA_REFERENCIA>2020-10-16T14:05:00</dtecomm:FECHA_REFERENCIA>
                                <dtecomm:VALIDAR_REFERENCIA_INTERNA>VALIDAR</dtecomm:VALIDAR_REFERENCIA_INTERNA>
                            </dtecomm:InformacionAdicional>
                        </dtecomm:Informacion_COMERCIAL>
                    </dte:Adenda>
                </dte:SAT>
            </dte:GTDocumento>';
        $this->data->xml_data = $xml_data;
    }
        
    private function prepare_nit() {
        $min_nit = $this->data->nit;
        $l_c = strlen ($min_nit);
        $txt_nit = "";
        if( $l_c < 12) {
            $kk = 12 - $l_c;
            for ( $i = 0; $i < $kk; $i++){
                $txt_nit = $txt_nit . "0";
            }
            $this->data->nit = $txt_nit.$min_nit;
        }else {
            $this->data->nit = $min_nit;
        }
        return $this->data->nit;
    }
}
