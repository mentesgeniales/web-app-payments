<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comercio;
use App\MonitorTranscation;
use Excel;
use Maatwebsite\Excel\Concerns\FromArray;

class ReportesController extends Controller
{
    //
    public function ventasByMonth($id, $year, $month){
        //$comercio = comercio::find($id);
        $transacciones = MonitorTranscation::where("comercio_id" , $id)->get();
        //dd($transacciones[120]);
        foreach ($transacciones as $key => $value) {

            $data[$key]["id"] = $value->transacction_id;
            if($value->log_data != null){
                $log_data = \GuzzleHttp\json_decode(($value->log_data));
                if(isset($log_data->cliente)){
                    $data[$key]["nombre_cliente"] = $log_data->cliente->firstName . " " . $log_data->cliente->lastName ;
                }
                $data[$key]["last_four"] = $value->last_four;
                $data[$key]["total_transaccion"] = $value->transaction_total;
                if(isset($log_data->tarjetaPagalo->nCuotas)){
                    switch ($log_data->tarjetaPagalo->nCuotas){
                        case 3:
                            $porcentaje = 8;
                        case 6:
                            $porcentaje = 9;
                        case 12:
                            $porcentaje = 10;
                    }
                    $data[$key]["tipo_transaccion"] = "CUOTAS" ;
                    $data[$key]["total_a_liquidar"] = ( $value->transaction_total - ( $value->transaction_total * ($porcentaje/100)) ) - 1.95;
                    $data[$key]["numero_de_cuotas"] = $log_data->tarjetaPagalo->nCuotas ;
                    $data[$key]["porcentaje_costo_banco"] = $porcentaje ;
                } else {
                    $data[$key]["tipo_transaccion"] = "CONTADO" ;
                    $data[$key]["total_a_liquidar"] = ( $value->transaction_total - ( $value->transaction_total * (6.5/100)) ) - 1.95 ;
                    $data[$key]["numero_de_cuotas"] = 0;
                    $data[$key]["porcentaje_costo_banco"] = 6.5 ;
                }
                $data[$key]["fecha"] = $value->date;
                if($value->full_response != null ){
                    $response_data = \GuzzleHttp\json_decode(($value->full_response));
                    if(isset($response_data->decision))
                        $data[$key]["resultado"] = $response_data->decision;
                }
            }
        }
        $export = new ExportReportsController($data);
        return Excel::download($export, 'reporte-transacciones.xlsx');


/*        return Excel::download($data, 'invoices.xlsx');
        return (new InvoicesExport)->download('invoices.xlsx', \Maatwebsite\Excel\Excel::XLSX);
        dd($data);
        return $log_data;
*/
    }
}
