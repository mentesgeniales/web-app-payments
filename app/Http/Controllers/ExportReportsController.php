<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportReportsController implements FromArray, WithHeadings
{
    //
    protected $transactions;

    public function __construct(array $transactions)
    {
        $this->transactions = $transactions;
    }
    public function headings(): array
    {
        return [
            'ID de Transacción',
            'Nombre Cliente',
            'Ultimos 4 CC',
            'Monto Total',
            'Tipo de Transacción',
            'Total a Liquidar',
            'Cantidad de Cuotas',
            'Comisión Bancaria',
            'Fecha',
            'Resultado Final',
        ];
    }
    public function array(): array
    {
        return $this->transactions;
    }
}
