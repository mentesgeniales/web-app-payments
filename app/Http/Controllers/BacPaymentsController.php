<?php

namespace App\Http\Controllers;

use App\BannedIp;
use App\Comercio;
use App\CompanyCredential;
use App\Consumer;
use App\CredencialesComercio;
use App\MonitorTranscation;
use Illuminate\Http\Request;
use SoapClient;
use SoapHeader;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class BacPaymentsController extends Controller {
    public function __construct() {
        $this->wc_api = "/wp-json/wc/v3/";
        $this->company_info = CompanyCredential::find(2);
    }
    function Sign($passwd, $facId, $acquirerId, $orderNumber, $amount, $currency) {
        $stringtohash = $passwd.$facId.$acquirerId.$orderNumber.$amount.$currency;
        $hash = sha1($stringtohash, true);
        return base64_encode($hash);
    }
    public function amexPay(Request $request){
        $data = base64_decode($request->hash);
        $t = explode("|", $data );
        $wsdlurl = 'https://marlin.firstatlanticcommerce.com/PGService/Services.svc?wsdl';
        $options = array(
            'location' =>'https://marlin.firstatlanticcommerce.com/PGService/Services.svc',
            'soap_version'=>SOAP_1_1,
            'exceptions'=>0,
            'trace'=>1,
            'cache_wsdl'=>WSDL_CACHE_NONE
        );
        $client = new SoapClient($wsdlurl , $options);
        $password = '37CBbjM6';
        $facId = '33302098';
        $acquirerId = '464748';
        $orderNumber = $t[1];
        $this->clean_comercio_id($t[0]);
        $cc = CredencialesComercio::where("comercio_id" ,"=",$this->comercio_id)->get();
        if(count ($cc) >= 1 ) {
            $order = $this->curl($this->comercio->site_url . $this->wc_api . "orders/" . $t[1] . "?consumer_key=" . $cc[0]->api_pub . "&consumer_secret=" . $cc[0]->api_pri);
            $amount = str_pad('' . ($order->total * 100), 12, "0", STR_PAD_LEFT);
            $currency = '320';
            $signature = $this->Sign($password, $facId, $acquirerId, $orderNumber, $amount, $currency);
            $CardDetails = array('CardCVV2' => $request->cvv,
                'CardExpiryDate' => $request->t1,
                'CardNumber' => $request->cc,
                'IssueNumber' => '',
                'StartDate' => '');
            $TransactionDetails = array('AcquirerId' => $acquirerId,
                'Amount' => $amount,
                'Currency' => $currency,
                'CurrencyExponent' => 2,
                'IPAddress' => '',
                'MerchantId' => $facId,
                'OrderNumber' => $orderNumber,
                'Signature' => $signature,
                'SignatureMethod' => 'SHA1',
                'TransactionCode' => '8');
            $AuthorizeRequest = array('Request' => array('CardDetails' => $CardDetails, 'TransactionDetails' => $TransactionDetails));
            $result = $client->Authorize($AuthorizeRequest);
            $data = [
                "name" => $order->billing->first_name . " " .$order->billing->last_name,
                "address" => $order->billing->address_1,
                "email" => $order->billing->email,
                "phone" => $order->billing->phone,
                "ip" => $this->get_the_user_ip(),
                "trx_type" => 1,
                "last_four" => substr ( $request->cc , -4),
                "total" => $order->total,
                "cvv2" => $request->cvv,
                "expDate" => $request->t1
            ];
            if (isset( $client->fault ) ) {
                $this->save_transaction($data,["estado" => 2], $result->AuthorizeResult->CreditCardTransactionResults);
                return ["responseCode" => 510 , "msg" => isset($result->AuthorizeResult->CreditCardTransactionResults) ? $result->AuthorizeResult->CreditCardTransactionResults->ReasonCodeDescription : "Error al procesar la transacción" ];
            } else {
                if(isset($client->error)){
                    $this->save_transaction($data,["estado" => 2], $result->AuthorizeResult->CreditCardTransactionResults);
                    return ["responseCode" => 510 , "msg" => isset($result->AuthorizeResult->CreditCardTransactionResults) ? $result->AuthorizeResult->CreditCardTransactionResults->ReasonCodeDescription : "Error desconocido al procesar la transacción" ];
                } else {
                    if($result->AuthorizeResult->CreditCardTransactionResults->ResponseCode == 3){
                        $this->save_transaction($data,["estado" => 2], $result->AuthorizeResult->CreditCardTransactionResults);
                        return ["responseCode" => 510 , "msg" => isset($result->AuthorizeResult->CreditCardTransactionResults) ? $result->AuthorizeResult->CreditCardTransactionResults->ReasonCodeDescription : "Error al procesar tarjeta" ];
                    }else if ( $result->AuthorizeResult->CreditCardTransactionResults->ResponseCode == 2 ){
                        $this->save_transaction($data,["estado" => 2], $result->AuthorizeResult->CreditCardTransactionResults);
                        return ["responseCode" => 510 , "msg" => isset($result->AuthorizeResult->CreditCardTransactionResults) ? $result->AuthorizeResult->CreditCardTransactionResults->ReasonCodeDescription : "Tarjeta denegada" ];
                    }else{
                        $this->save_transaction($data,["estado" => 1], $result->AuthorizeResult->CreditCardTransactionResults);
                        $url = $this->comercio->site_url . $this->wc_api . "orders/" . $t[1] . "?consumer_key=" . $cc[0]->api_pub . "&consumer_secret=" . $cc[0]->api_pri;
                        $data = array("status" => "processing" , "note" =>  "Pago completado vía Central de Pago");
                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
                        $response = curl_exec($ch);
                        $r = json_decode($response);
                        return ["responseCode" => 200, "url" => $this->comercio->site_url . "/finalizar-compra/order-received/" . $t[1] ."/?key=" . $r->order_key];
                    }
                }
            }
        }
    }

    public function getPaymentScreen( $paymentData ){
        $data = base64_decode($paymentData);
        $t = explode("|",$data );
        $this->clean_comercio_id($t[0]);
        

        $cc = CredencialesComercio::where("comercio_id" ,"=",$this->comercio_id)->get();
        $comercio_name = $this->comercio->name;
        if(count ($cc) >= 1 ){
            $order = (object)  $this->curl($this->comercio->site_url . $this->wc_api . "orders/" . $t[1] . "?consumer_key=" .$cc[0]->api_pub . "&consumer_secret=" . $cc[0]->api_pri );
            $order->billing = (object)  $order->billing;
            //dd();
            $status = $order->status;
            $amountFormatted = str_pad(''.( $order->total*100), 12, "0", STR_PAD_LEFT);
            $currency = '320';
            //dd($this->Sign('37CBbjM6', '33302098', '464748', $t[1], $amountFormatted, $currency));
            return view("localpay" , [
                "hash" => $paymentData,
                "totalNormal" => $order->total,
                "comercio_name" => $comercio_name ,
                "no_orden" => $t[1],
                "first_name" => $order->billing->first_name,
                "last_name" => $order->billing->last_name,
                "address_l1" => $order->billing->address_1,
                "address_l2" => $order->billing->address_2,
                "municipio" => $order->billing->city,
                "departamento" => $order->billing->state,
                "codigo_postal" => $order->billing->postcode,
                "email" => $order->billing->email,
                "phone" => $order->billing->phone,
                "total" => $amountFormatted,
                "ip" => $this->get_the_user_ip(),
                "statusPago " => "1","display" =>"none",
                "status" => $status, "facId" => '33302098' ,"acquiredId" => "464748",
                "signature" => $this->Sign('37CBbjM6', '33302098', '464748', $t[1], $amountFormatted, $currency)
            ]);
        }
    }

    public function getCompletedScreen(Request $request){
        //dd($request);
        $data = str_replace("/completado", "", str_replace("f/","",$request->path()));
        $data = base64_decode($data);
        $t = explode("|",$data );
        $this->clean_comercio_id($t[0]);
        $cc = CredencialesComercio::where("comercio_id" ,"=",$this->comercio_id)->get();
        $order = (object) $this->curl($this->comercio->site_url . $this->wc_api . "orders/" . $t[1] . "?consumer_key=" .$cc[0]->api_pub . "&consumer_secret=" . $cc[0]->api_pri );
        $order->billing = (object)  $order->billing;
        $f = [];
        $df = [
            "name" => $order->billing->first_name . " " .$order->billing->last_name,
            "address" => $order->billing->address_1,
            "email" => $order->billing->email,
            "phone" => $order->billing->phone,
            "ip" => $this->get_the_user_ip(),
            "trx_type" => 1,
            "last_four" => 9999,
            "total" => $order->total,
            "cvv2" => $request->CVV2Result,
            "expDate" => $request->t1
           ];
        if( $request->ReasonCode == 3 ){
            return ["responseCode" => 515 , "msg" => isset($response->mensaje) ? $request->ReasonCodeDesc : "No hay mensaje de error" ];
        }
        if ( $request->ReasonCode == 1  ) {
            $f["ResponseCode"] = $request->ReasonCode;
            $f["ReasonCodeDescription"] = $request->ReasonCodeDesc;
            $this->save_transaction($df, ["estado" => 1], (object) $f);
            $url = $this->comercio->site_url . $this->wc_api . "orders/" . $t[1] . "?consumer_key=" . $cc[0]->api_pub . "&consumer_secret=" . $cc[0]->api_pri;
            $data = array("status" => "processing" , "note" =>  "Pago completado vía Central de Pago");
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
            $response = curl_exec($ch);
            $r = json_decode($response);
            return redirect($this->comercio->site_url . "/finalizar-compra/order-received/" . $t[1] ."/?key=" . $r->order_key);
        } else {
            $this->ban_ip($this->get_the_user_ip());
            $comercio_name = $this->comercio->name;
            $status = $order->status;
            $amountFormatted = str_pad(''.( $order->total*100), 12, "0", STR_PAD_LEFT);
            $currency = '320';
            $f["ResponseCode"] = $request->ReasonCode;
            $f["ReasonCodeDescription"] = $request->ReasonCodeDesc;
            $this->save_transaction($df,["estado" => 2], (object)  $f);
            return view("localpay" , [
                "hash" => base64_encode($data),
                "totalNormal" => $order->total,
                "comercio_name" => $comercio_name ,
                "no_orden" => $t[1],
                "first_name" => $order->billing->first_name,
                "last_name" => $order->billing->last_name,
                "address_l1" => $order->billing->address_1,
                "address_l2" => $order->billing->address_2,
                "municipio" => $order->billing->city,
                "departamento" => $order->billing->state,
                "codigo_postal" => $order->billing->postcode,
                "email" => $order->billing->email,
                "phone" => $order->billing->phone,
                "total" => $amountFormatted,
                "ip" => $this->get_the_user_ip(),
                "status" => $status, "facId" => '33302098' ,"acquiredId" => "464748",
                "statusPago" => 2,"display" =>"yes",
                "signature" => $this->Sign('37CBbjM6', '33302098', '464748', $t[1], $amountFormatted, $currency)
            ]);
        }
    }

    public function ban_ip($ip_array){
        $ip_array = explode(",", str_replace(" ","", $ip_array ));
        foreach ($ip_array as $key => $value) {
            $banned_ips = BannedIp::where("ip", $value)->Today()->get();
            if(count($banned_ips) >=1 ){
                $banned_ips[0]->level =  $banned_ips[0]->level + 1;
                $banned_ips[0]->site_url = $this->comercio->site_url;
                $banned_ips[0]->comercio_id = $this->comercio_id;
                $banned_ips[0]->save();
            }else{
                $banned_ips = new BannedIp;
                $banned_ips->site_url = $this->comercio->site_url ;
                $banned_ips->ip = $value;
                $banned_ips->comercio_id = $this->comercio_id;
                $banned_ips->level = 1;
                $banned_ips->save();
                return true;
            }
        }
    }

    public function clean_comercio_id ($comercio_id){
        $this->comercio_id = str_replace( "GT2020100" , "", $comercio_id);
        $this->comercio_id = (int) $this->comercio_id;
        $this->comercio = Comercio::find( $this->comercio_id);
    }

    private function save_transaction($d, $r, $f)  {
        Log::info("1");
        $trx = new MonitorTranscation();
        $trx->tipo = $d["trx_type"];
        $trx->comercio_id = $this->comercio_id;

        if(isset($r->requestID)) {
            $trx->transacction_id = $r->requestID;
        }else{
            $trx->transacction_id = 20201520;
        }

        $c = Consumer::where("email" ,$d["email"] )->get();
        if( count($c) >= 1 ) {
            $trx->consumer_id = $c[0]->id;
        }else{
            $c = new Consumer();
            $c->full_name = $d["name"];
            $c->email = $d["email"];
            $c->phone = $d["phone"];
            $c->address = $d["address"];
            $c->ip =$d["ip"];
            $c->save();
            $trx->consumer_id = $c->id;
        }
        $trx->last_four = $d["last_four"];
        $trx->transaction_total = $d["total"];
        $trx->date = Carbon::now();
        if(isset($r["estado"])){
            $trx->response_status = $r["estado"];
        }else{
            $trx->response_status = "2";
        }

        Log::info("2");
        $this->data["tarjetaPagalo"]["accountNumber"] = "accountNumber";
        $trx->log_data = json_encode($d);
        $trx->respuesta = isset($f->ResponseCode) ? $f->ReasonCodeDescription  : "no hay respuesta";
        $trx->id_codigo_interno = isset($f->ResponseCode) ? $f->ResponseCode  : 203;
        $trx->full_response = json_encode($f);
        $trx->save();
        Log::info("3");
    }
}
