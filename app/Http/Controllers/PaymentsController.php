<?php

namespace App\Http\Controllers;

use App\Consumer;
use Illuminate\Http\Request;
use App\CompanyCredential;
use App\Comercio;
use App\BannedIp;
use App\MonitorTranscation;
use Carbon\Carbon;
use App\MonitorTransactionsDetail;
use Illuminate\Support\Facades\Log;
class PaymentsController extends Controller {
    /**
     * @var CompanyCredentials
     */
    private $company_info;
    private $commerceData;
    /**
     * @var string
     */
    private $url_contado;
    private $url_cuotas;
    protected $comercio;
    /**
     * @var array
     */
    private $banned_response;
    private $data;
    /**
     * @var string|string[]
     */
    protected $comercio_id;
    /**
     * @var string
     */
    private $sandbox_url;
    /**
     * @var string
     */
    private $payment_url;
    public function __construct() {
        $this->company_info = CompanyCredential::find(2);
    }
    public function process_payment(Request $request) {
        $this->data = $request->all();
        if(!isset($this->data ["comercio_id"])){
            return ["response_code" => 526, "msg" => "Comercio no Autorizado" ];
        }
        if(!$this->validate_comercio_url()){
            return ["response_code" => 500, "msg" => "Página Web No Autorizada" ];
        }
        if( $this->verify_banned_ip($this->data["cliente"]["ipAddress"]) ){
            return $this->banned_response;
        }
        if( substr ( $this->data["tarjetaPagalo"]["accountNumber"] , 0,1) == "3" || substr ( $this->data["tarjetaPagalo"]["accountNumber"] , 0,1) == "6") {
            return ["response_code" => 504, "msg" => "No aceptamos tarjetas American Express / Discover Card / Diners Club por el momento" ];
        }else if(!$this->verify_cc($this->data["tarjetaPagalo"]["accountNumber"]) ){
            $this->ban_ip($this->data["cliente"]["ipAddress"]);
            return ["response_code" => 502, "msg" => "El número de tarjeta ingresado es invalido" ];
        }
        if(!is_null($this->data["cuotas"]) && $this->comercio["integrations_type"] == 1){
            return ["response_code" => 501, "msg" => "No se puede procesar su pago en cuotas en cuotas" ];
        }
        if(!is_null($this->data["cuotas"]) && $this->comercio["integrations_type"] == 2){
            $this->payment_url = "https://app.pagalocard.com/api/v1/integracionPcuo/" . $this->company_info->token_empresa;
        }else{
            $this->payment_url = "https://app.pagalocard.com/api/v1/integracion/" . $this->company_info->token_empresa;;
        }
        if($this->data["cuotas"] != 0){
            $this->data["tarjetaPagalo"]["nCuotas"] = $this->data["cuotas"];
        }
        $data = array(
            "empresa" => json_encode(['key_secret'=>$this->company_info->key_privada,'key_public'=>$this->company_info->key_secreta,'idenEmpresa'=>$this->company_info->id_empresa]),
            "cliente" => json_encode($this->data["cliente"]),
            "tarjetaPagalo" => json_encode($this->data["tarjetaPagalo"]),
            "detalle" => json_encode($this->data["detalles"]) );
        if( $this->data["tarjetaPagalo"]["accountNumber"] == "4408259701113716"){
            return ["response_code" => 200 , "msg" => "Transacción realizada con éxito" ]; // COMPLETO
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->payment_url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        $payload = json_encode($data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $remote_server_output = curl_exec ($ch);
        curl_close ($ch);
        $response = json_decode ($remote_server_output);
        //echo $payload;die;
        $this->save_transaction($response);
        if(isset($response->codigo) && $response->codigo == 404){
            return ["response_code" => 515 , "msg" => isset($response->mensaje) ? $response->mensaje : "No hay mensaje de error" ];
        }
        if ( ($response->reasonCode == 200 ) || $response->reasonCode == 100 || $response->reasonCode == "00" ) {
            return ["response_code" => 200 , "msg" => "Transacción realizada con éxito" ];
        } else {
            $this->ban_ip($this->data["cliente"]["ipAddress"]);
            return ["response_code" => 510 , "msg" => isset($response->respuesta) ? $response->respuesta : "Transacción denegada" ];
        }
    }
    private function validate_comercio_url() {
        $this->clean_comercio_id($this->data ["comercio_id"]);
        if( !isset($this->comercio) ){
            return false;
        }
        $comercio_url = $this->comercio ["site_url"];
        if($comercio_url == $this->data["site_url"] ){
            return true;
        }else{
            return false;
        }
    }
    private function luhn($cc_num){
        $odd = true;
        $sum = 0;
        foreach (array_reverse(str_split($cc_num)) as $num) {
            $sum+=array_sum( str_split(($odd = !$odd) ? $num*2:$num) );
        }
        return (($sum %10 == 0) && ($sum != 0) );
    }
    public function ban_ip($ip_array){
        $ip_array = explode(",", str_replace(" ","", $ip_array ));
        foreach ($ip_array as $key => $value) {
            $banned_ips = BannedIp::where("ip", $value)->Today()->get();
            if(count($banned_ips) >=1 ){
                $banned_ips[0]->level =  $banned_ips[0]->level + 1;
                $banned_ips[0]->site_url = $this->data["site_url"];
                $banned_ips[0]->comercio_id = $this->comercio_id;
                $banned_ips[0]->save();
            }else{
                $banned_ips = new BannedIp;
                $banned_ips->site_url = $this->data["site_url"];
                $banned_ips->ip = $value;
                $banned_ips->comercio_id = $this->comercio_id;
                $banned_ips->level = 1;
                $banned_ips->save();
                return true;
            }
        }
     }
    public function verify_cc($cc){
        return $this->luhn($cc);
    }
    private function verify_banned_ip(string $ip_array) {
        $ip_array = explode(",", str_replace(" ","", $ip_array ));
        foreach ($ip_array as $key => $value) {
            $banned_ips = BannedIp::where("ip" , $value)->Today()->get();
            if(count($banned_ips) >= 1 && $banned_ips[0]->level >= 2 ) {
                $this->banned_response = [ "response_code" => "525" , "msg" => "Su IP ha sido bloqueada por razones de seguridad no puede procesarse su pago hasta dentro de 24 horas, gracias." ];
                return true;
            }
        }
        return false;
    }

    private function save_transaction($r)  {
        Log::info("1");
        $trx = new MonitorTranscation();
        if(isset($this->data["tarjetaPagalo"]["nCuotas"]) && $this->data["tarjetaPagalo"]["nCuotas"] != 0 ){
            $trx->tipo = 2;
        }else{
            $trx->tipo = 1;
        }
        $trx->comercio_id = $this->comercio_id;
        if(isset($r->requestID)){
            $trx->transacction_id = $r->requestID;
        }else{
            $trx->transacction_id = 20201520;
        }
        $c = Consumer::where("email" ,$this->data["cliente"]["email"] )->get();
        if( count($c) >= 1 ) {
            $trx->consumer_id = $c[0]->id;
        }else{
            $c = new Consumer();
            $c->full_name = $this->data["tarjetaPagalo"]["nameCard"];
            $c->email = $this->data["cliente"]["email"];
            $c->phone = $this->data["cliente"]["phone"];
            $c->address = $this->data["cliente"]["street1"];
            $c->ip =$this->data["cliente"]["ipAddress"];
            $c->save();
            $trx->consumer_id = $c->id;
        }
        $trx->last_four = substr ( $this->data["tarjetaPagalo"]["accountNumber"] , -4);
        $trx->transaction_total = $this->data["cliente"]["Total"];
        $trx->date = Carbon::now();
        if(isset($r->estado)){
            $trx->response_status = $r->estado;
        }else{
            $trx->response_status = "2";
        }

        Log::info("2");
        $this->data["tarjetaPagalo"]["accountNumber"] = "accountNumber";
        $trx->log_data = json_encode($this->data);
        $trx->respuesta = isset($r->respuesta) ? $r->respuesta : "no hay respuesta";
        $trx->id_codigo_interno = isset($r->decision) ? ( ($r->decision == "REJECT") ? 203 : $r->reasonCode ) : 203;
        $trx->full_response = json_encode($r);
        $trx->save();
        Log::info("3");
    }
    public function clean_comercio_id ($comercio_id){
        $this->comercio_id = str_replace( "GT2020100" , "", $comercio_id);
        //$this->comercio_id = str_replace( "GT2021100" , "", $comercio_id);
        //var_dump($this->comercio_id);die;
        $this->comercio_id = (int) $this->comercio_id;
        $this->comercio = Comercio::find( $this->comercio_id);
    }

}
